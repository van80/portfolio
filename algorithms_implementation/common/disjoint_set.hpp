#pragma once

#include <vector>
#include <algorithm>
#include <iterator>
#include <functional>

namespace common {

class disjoint_set
{
public: // types

	typedef size_t rank_type;
	typedef size_t size_type;

	struct ref_rank
	{
		size_type parent;
		rank_type rank;
	};

public: // methods

	disjoint_set( size_type element_count, size_type min_vertex = 1 );

	size_type count_of_elements() const { return m_elements.size() - m_min_vertex; }

	size_type count_of_sets() const;

	rank_type rank( size_type elem ) const;

	size_type find( size_type elem ) const; // does not modify the abstract object, however internals are changed

	bool in_same_set( size_type el1, size_type el2 ) const;

	void join( size_type el1, size_type el2 );

private: // methods

private: // fields

	size_type m_count_of_sets;
	const size_type m_min_vertex;
	mutable std::vector< ref_rank > m_elements;


}; // class disjoint_set

} // namespace common
