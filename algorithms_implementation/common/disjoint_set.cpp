#include <disjoint_set.hpp>

#include <iostream>

#include <dbc.hpp>

namespace {

typedef common::disjoint_set::size_type size_type;
typedef common::disjoint_set::rank_type rank_type;
typedef std::vector< size_type > stack;

} // private namespace

namespace common {

//--------------------------------------------------------------------------------

disjoint_set::disjoint_set( size_type element_count, size_type min_vertex )
	: m_count_of_sets( element_count )
	, m_min_vertex( min_vertex )
{
	m_elements.reserve( element_count + min_vertex );
	size_type counter = 0;
	std::generate_n( std::back_inserter( m_elements ), m_elements.capacity(), [&counter]()
	{
		ref_rank elem;
		elem.parent = counter;
		elem.rank = 0;
		++counter;
		return elem;
	});
}

//--------------------------------------------------------------------------------

size_type disjoint_set::count_of_sets() const
{
	return m_count_of_sets;
}

//--------------------------------------------------------------------------------

rank_type disjoint_set::rank( size_type elem ) const
{
	return m_elements.at( elem ).rank;
}

//--------------------------------------------------------------------------------

size_type disjoint_set::find( size_type elem ) const // does not modify the abstract object, however internals are changed
{
	PRECONDITION( elem_is_within_range, (m_min_vertex <= elem) && (elem <= m_elements.size() ) );

	ref_rank& rr = m_elements.at( elem );
	const size_type p = rr.parent;
	const size_type lead = elem != p ? find( p ) : p;

	if ( lead != p )
	{
		rr.parent = lead;
	}

	// it is impossible that rank of lead is smaller than rank of the element
	POSTCONDITION( rank_is_increasing_towards_lead, rr.rank <= rank( lead ) );

	return lead;
}

//--------------------------------------------------------------------------------

bool disjoint_set::in_same_set( size_type el1, size_type el2 ) const
{
	return find( el1 ) == find( el2 );
}

//--------------------------------------------------------------------------------

void disjoint_set::join( size_type el1, size_type el2 )
{
	size_type p1 = find( el1 );
	size_type p2 = find( el2 );

	PRECONDITION( sets_are_different, p1 != p2 );

	if ( rank( p1 ) < rank( p2 ) )
	{
		// reparent to p2
		ref_rank& rr1 = m_elements.at( p1 );
		rr1.parent = p2;

	}
	else if ( rank( p2 ) < rank( p1 ) )
	{
		// reparent to p1
		ref_rank& rr2 = m_elements.at( p2 );
		rr2.parent = p1;
	}
	else // resolve ties
	{
		// reparent to p1 and increase its rank
		ref_rank& rr1 = m_elements.at( p1 );
		++rr1.rank;

		ref_rank& rr2 = m_elements.at( p2 );
		rr2.parent = p1;
	}

	--m_count_of_sets;
}

} // namespace common
