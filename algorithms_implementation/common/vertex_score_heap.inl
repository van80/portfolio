#pragma once

#include <iterator>
#include <limits>

#include <dbc.hpp>


namespace common {


namespace internal {

//--------------------------------------------------------------------------------
template < class TVertex, class TScore >
inline const TVertex& vertex( const std::pair< TVertex, TScore >& vs )
{
	return vs.first;
}

//--------------------------------------------------------------------------------
template < class TVertex, class TScore >
inline TVertex& vertex( std::pair< TVertex, TScore >& vs )
{
	return vs.first;
}

//--------------------------------------------------------------------------------
template < class TVertex, class TScore >
inline const TScore& score( const std::pair< TVertex, TScore >& vs )
{
	return vs.second;
}

//--------------------------------------------------------------------------------
template < class TVertex, class TScore >
inline TScore& score( std::pair< TVertex, TScore >& vs )
{
	return vs.second;
}

//--------------------------------------------------------------------------------

template < class It >
inline It parent( const It& begin, const It& it )
{
	auto d = std::distance( begin, it );
	return std::next( begin, (d - 1) / 2 );
}

//--------------------------------------------------------------------------------

template < class It >
It left_child( const It& begin, const It& it, const It& end )
{
	auto n = 2 * std::distance( begin, it ) + 1;
	return n < std::distance( begin, end ) ? std::next( begin, n ) : end;
}

} // namespace internal


//================================================================================

template < class TScoreType, class TComparator >
const typename vertex_score_heap< TScoreType, TComparator >::size_type 
				vertex_score_heap< TScoreType, TComparator >::bad_index 
								= std::numeric_limits< size_type >::max();

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
vertex_score_heap< TScoreType, TComparator >::vertex_score_heap( size_type capacity )
{
	m_heap.reserve( capacity );
	m_vertex_map.resize( capacity + 1, bad_index ); // +1 because we will have an unused index 0 for our convenience
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
bool vertex_score_heap< TScoreType, TComparator >::contains( vertex_type vertex ) const
{
	size_type pos = m_vertex_map.at( vertex );
	return pos != bad_index;
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
typename vertex_score_heap< TScoreType, TComparator >::score_type vertex_score_heap< TScoreType, TComparator >::score( vertex_type vertex ) const
{
	PRECONDITION( vertex_within_bounds, (1 <= vertex) && (vertex <= capacity()) );

	size_type pos = m_vertex_map.at( vertex );

	PRECONDITION( vertex_is_in_heap, pos != bad_index );

	return internal::score( m_heap.at( pos ) );
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
typename vertex_score_heap< TScoreType, TComparator >::score_type vertex_score_heap< TScoreType, TComparator >::score( 
			vertex_type vertex, 
			score_type error_score 
		) const
{
	PRECONDITION( vertex_within_bounds, (1 <= vertex) && (vertex <= capacity()) );

	size_type pos = m_vertex_map.at( vertex );

	return pos != bad_index ? internal::score( m_heap.at( pos ) ) : error_score;
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
void vertex_score_heap< TScoreType, TComparator >::push( vertex_type vertex, score_type score )
{
	typedef typename heap_t::iterator It;

	PRECONDITION( not_full, !full() );
	PRECONDITION( vertex_within_allowed_range, (0 < vertex) && (vertex <= capacity()) );
	PRECONDITION( no_duplicates_allowed, !contains( vertex ) );

	const size_type old_size = size();

	m_heap.push_back( vertex_score( vertex, score ) );
	m_vertex_map.at( vertex ) = m_heap.size() - 1;

	bubble_up( std::prev( m_heap.end() ) );

	POSTCONDITION( size_increased, size() == old_size + 1 );
	POSTCONDITION( heap_contain_vertex, ::common::internal::vertex( m_heap.at( m_vertex_map.at( vertex ) ) ) == vertex );
	POSTCONDITION( heap_contain_score, ::common::internal::score( m_heap.at( m_vertex_map.at( vertex ) ) ) == score );
}

//--------------------------------------------------------------------------------
template < class TScoreType, class TComparator >
bool vertex_score_heap< TScoreType, TComparator >::bubble_up( const typename heap_t::iterator& initial_it )
{
	typedef typename heap_t::iterator It;
	static const vertex_score_cmp less;
	int swap_count = 0;

	if ( initial_it != m_heap.begin() )
	{
		It it = initial_it;

		CREATE_LOOP_VARIANT( iterator_bubles_up, std::distance( m_heap.begin(), it ) );

		// we cannot change it up to m_heap.begin() because it's parent will become undefined in the last iteration
		do
		{
			UPDATE_LOOP_VARIANT( iterator_bubles_up );

			It p = internal::parent( m_heap.begin(), it );

			if ( !less( *p, *it ) )
				break;

			swap( p, it );

			++swap_count;

			it = p;

		}  while ( (m_heap.begin() != it) ); // while order is not restored

	} // if not the top_most element in the heap

	// we limit ourself by two checks only for better performance
	POSTCONDITION( local_order_is_restored, (initial_it == m_heap.begin()) || !less( *internal::parent( m_heap.begin(), initial_it ), *initial_it ) );
	POSTCONDITION( global_order_is_restored, (initial_it == m_heap.begin()) || !less( m_heap.front(), *internal::parent( m_heap.begin(), initial_it ) ) );

	return swap_count > 0;
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
void vertex_score_heap< TScoreType, TComparator >::swap( typename heap_t::iterator it1, typename heap_t::iterator it2 )
{
	m_vertex_map.at( internal::vertex( *it1 ) ) = std::distance( m_heap.begin(), it2 );
	m_vertex_map.at( internal::vertex( *it2 ) ) = std::distance( m_heap.begin(), it1 );
	std::swap( *it1, *it2 );
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
void vertex_score_heap< TScoreType, TComparator >::remove( vertex_type vertex )
{
	typedef typename heap_t::iterator It;
	static const vertex_score_cmp less;

	// additional preconditions following from the one declared in the interface
	PRECONDITION( not_empty, !empty() );
	PRECONDITION( vertex_within_allowed_range, (0 < vertex) && (vertex <= capacity()) );

	size_type pos = m_vertex_map.at( vertex );
	PRECONDITION( contains_vertex, pos != bad_index ); // our interface precondition

	It tail = std::prev( m_heap.end() );
	It it = std::next( m_heap.begin(), pos );

	if ( it != tail )
	{
		swap( it, tail );
		m_vertex_map.at( vertex ) = bad_index;
	}

	bubble_up( it ) || max_heapify( it, tail );

	m_heap.pop_back();
	m_vertex_map.at( vertex ) = bad_index;

	POSTCONDITION( vertex_removed, !contains( vertex ) );
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
void vertex_score_heap< TScoreType, TComparator >::get_top( vertex_type& vertex, score_type& score ) const
{
	PRECONDITION( not_empty, !empty() );
	vertex = internal::vertex( m_heap.front() );
	score = internal::score( m_heap.front() );
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
void vertex_score_heap< TScoreType, TComparator >::pop()
{
	typedef typename heap_t::iterator It;

	PRECONDITION( not_empty, !empty() );

	const size_type old_size = size();
	const vertex_type vertex = internal::vertex( m_heap.front() );

	const It tail = std::prev( m_heap.end() );

	if ( m_heap.begin() != tail ) // not the only element in the heap
	{
		swap( tail, m_heap.begin() );
		max_heapify( m_heap.begin(), tail );
	}

	m_heap.pop_back();
	m_vertex_map.at( vertex ) = bad_index;

	POSTCONDITION( size_decreased, old_size == size() + 1 );
	POSTCONDITION( vertex_index_is_invalidated, m_vertex_map.at( vertex ) == bad_index );
	POSTCONDITION( top_is_updated, empty() || m_vertex_map.at( internal::vertex( m_heap.front() ) ) == 0 );
}

//--------------------------------------------------------------------------------

template < class TScoreType, class TComparator >
bool vertex_score_heap< TScoreType, TComparator >::max_heapify( const typename heap_t::iterator& initial_it, const typename heap_t::iterator& end )
{
	typedef typename heap_t::iterator It;
	static const vertex_score_cmp less;

	const It top_left_child = internal::left_child( m_heap.begin(), initial_it, end );
	//const It top_left_child = internal::left_child( initial_it, initial_it, end ); // <--- that was the reason of the bug !!!
	const It top_right_child = top_left_child != end ? std::next( top_left_child ) : end;

	It p = initial_it;
	It l = top_left_child;
	It last_leaf = l;

	CREATE_LOOP_VARIANT( approaching_to_leafs, std::distance( l, end ) );

	while ( l != end )
	{
		UPDATE_LOOP_VARIANT( approaching_to_leafs );

		last_leaf = l;

		It max_child = l;
		const It r = std::next( l );
		if ( (r != end) && less(*l, *r) )
		{
			max_child = r;
		}

		if ( !less( *p, *max_child ) )
		{
			break;
		}

		swap( p, max_child );

		p = max_child;
		l = internal::left_child( m_heap.begin(), p, end );
		//l = internal::left_child( initial_it, p, end ); // <--- that was the reason of the bug !!!
	}

	POSTCONDITION( local_order_with_left_child_is_restored, (top_left_child == end) || !less( *initial_it, *top_left_child ) );
	POSTCONDITION( local_order_with_right_child_is_restored, (top_right_child == end) || !less( *initial_it, *top_right_child ) );
	POSTCONDITION( global_order_is_restored, (last_leaf == end) || !less( *initial_it, *last_leaf ) );

	return p != initial_it;

} // push_heap

}; // class vertex_score_heap

