#pragma once

#include <vector>
#include <functional>

namespace common {

//================================================================================


// Some algorithms on graphs can be optimized if we use a heap in them.
// We cannot use the STL implementation of the heap because it lacks the following functionalities
// 1. Removal of an element at arbitrary position
// 2. Quick O(1) search for a value stored in the heap
// These were the reasons why we had to create our own implementation.

// Implemented as a max heap but the default order makes it min heap
// by default we have a min heap by score
template < class TScoreType, class TComparator = std::greater<TScoreType> >
class vertex_score_heap 
{
public: // types

	typedef size_t size_type;

	typedef size_t vertex_type;

	typedef TScoreType score_type;

	typedef TComparator comparator_type;

	typedef std::pair< size_type, score_type > vertex_score;

	typedef std::vector< vertex_score > heap_t;

	struct vertex_score_cmp
	{
		bool operator()( const vertex_score& vs1, const vertex_score& vs2 ) const
		{
			static const comparator_type cmp;
			return cmp( vs1.second, vs2.second );
		}
	};

public:

	//! Create a heap with given maximal size
	vertex_score_heap( size_type capacity );

	//! Maximal amount of elements we can store in the heap
	size_type capacity() const { return m_heap.capacity(); }

	//! Current amount of elements we can store in the heap
	size_type size() const { return m_heap.size(); }

	//! Is the heap empty?
	bool empty() const { return m_heap.empty(); }

	//! Is the heap full?
	bool full() const { return size() >= capacity(); }

	//! Iterator pointing to the top of the heap
	typename heap_t::const_iterator begin() const { return m_heap.begin(); }

	//! Iterator pointing after the last element of the heap
	typename heap_t::const_iterator end() const { return m_heap.end(); }

	//! Score of the vertex - the value defining order of elements. Running time - O(1).
	/*!
	 * \pre 1 <= vertex <= size
	 * \pre contains( vertex )
	 */
	score_type score( vertex_type vertex ) const;

	//! Score of the vertex. If the vertex is not represented in the heap - return error_score
	/*!
	 * \pre 1 <= vertex <= size
	 */
	score_type score( vertex_type vertex, score_type error_score ) const;

	//! Does the heap contain the vertex given? Runing time - O(1).
	/*!
	 * \pre 1 <= vertex <= size
	 */
	bool contains( vertex_type vertex ) const;

	//! Top-most element of the heap. Runing time - O(log n).
	/*!
	 * \pre not empty
	 */
	void get_top( vertex_type& vertex, score_type& score ) const;

	//! Add a new entry to the heap. No duplicates allowed. Running time - O(log n)
	/*!
	 * \pre 1 <= vertex <= capacity
	 * \pre not full
	 * \pre not contains( vertex )
	 * \post size = old.size + 1
	 * \post contains( vertex )
	 * \post score( vertex ) == score
	 */
	void push( vertex_type vertex, score_type score );

	//! Remove vertex from heap. Runing time - O(log n).
	/*!
	 * \pre contains( vertex )
	 * \post not contains( vertex )
	 */
	void remove( vertex_type vertex );

	//! Remove the top-most heap entry.
	/*!
	 * \pre not empty
	 * \post size = old.size - 1
	 * \post not contains( old.top_vertex )
	 */
	void pop();

private: // methods

	// swap iterators and update the mapping vertex -> score
	void swap( typename heap_t::iterator it1, typename heap_t::iterator it2 );

	// build the heap out of an array
	bool max_heapify( const typename heap_t::iterator& initial_it, const typename heap_t::iterator& end );

	// bubble up the heap entry (referred by the iterator) and update the mapping vertex -> score
	bool bubble_up( const typename heap_t::iterator& initial_it );

private:

	// heap with score being the ordering criteria
	heap_t m_heap;

	// map: vertex -> score
	std::vector< size_type > m_vertex_map;

	static const size_type bad_index;

}; // class vertex_score_heap


} // namespace common

#include <vertex_score_heap.inl>
