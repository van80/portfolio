#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

#include <dbc.hpp>

typedef unsigned long weight_t;

//================================================================================

struct node
{
	node() : w( 0 ), min_d( 0 ), max_d( 0 ) {}

	weight_t w; // weight
	size_t min_d; // minimal_depth
	size_t max_d; // maximal_depth
};

//================================================================================

typedef std::vector< node > vec;
typedef vec::iterator iter;

//================================================================================

//--------------------------------------------------------------------------------

/*
 * If we had to build the Huffman tree this operation would be responsible for joining two subtrees under the new parent node.
 * However in this problem our goal is only to find min_depth and max_depth characteristics of the tree.
 * Therefore we only mimic creation of the parent node while actually we only aggregate the needed information from the children.
 * The min depth of the resulting node is the minimal depth amoung its children + 1
 * The same is true for the max depth.
 */
node join( const node& n1, const node& n2 )
{
	node res;
	res.w = n1.w + n2.w;
	res.min_d = std::min( n1.min_d, n2.min_d ) + 1;
	res.max_d = std::max( n1.max_d, n2.max_d ) + 1;
	return res;
}

//--------------------------------------------------------------------------------

std::ostream& operator<< ( std::ostream& os, const node& n )
{
	os << "{weight=" << n.w << ", min_depth=" << n.min_d << ", max_depth=" << n.max_d << "}";
	return os;
}

//--------------------------------------------------------------------------------

void load_statistics( std::istream& input, vec& t )
{
	int amount;
	input >> amount;
	t.reserve( amount );

	std::transform( std::istream_iterator< weight_t >( input ), std::istream_iterator< weight_t >(), std::back_inserter( t ), []( weight_t w )
		{
			node n;
			n.w = w;
			// the other fields are initialized to 0 by the default constructor
			return n;
		});
}

//--------------------------------------------------------------------------------

void assemble_and_reduce( vec& t )
{
	// We need to retrieve nodes with minimum weights.
	// As STL provides us with max_heap only we have to cure this via inverted comparator
	auto less = []( const node& n1, const node& n2 ){ return n2.w < n1.w; };

	std::make_heap( t.begin(), t.end(), less );

	// In each loop iteration we take two lightest items, merge them into one
	// and push the result back to the heap
	// If we do things right the heap must shrink with each iteration
	CREATE_LOOP_VARIANT( heap_decreases, t.size() );

	while ( t.size() >= 2 )
	{
		UPDATE_LOOP_VARIANT( heap_decreases );

		node n1 = t.front();
		std::pop_heap( t.begin(), t.end(), less ); t.pop_back();

		node n2 = t.front();
		std::pop_heap( t.begin(), t.end(), less ); t.pop_back();
	
		t.push_back( join( n1, n2 ) );
		std::push_heap( t.begin(), t.end(), less );
	}
}

//--------------------------------------------------------------------------------

int main()
{
	vec t;
	load_statistics( std::cin, t );

	// We do not validate whether the input array is empty or not because this cannot be the case by problem definition
	
	auto min_max = std::minmax_element( t.begin(), t.end(), []( const node& n1, const node& n2 ){ return n1.w < n2.w; } );

	node min_st = *(min_max.first);
	node max_st = *(min_max.second);

	// just self check
	std::cout << "stats.size=" << t.size() << std::endl;
	std::cout << "min_stat=" << min_st << std::endl;
	std::cout << "max_stat=" << max_st << std::endl;

	assemble_and_reduce( t );

	// this is the value we need to find
	std::cout << "top_node=" << t.front() << std::endl;
	
	return 0;
}
