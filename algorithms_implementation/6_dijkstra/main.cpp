#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <iterator>

#include <dbc.hpp>
#include <vertex_score_heap.hpp>

typedef std::vector< int > vec;
typedef vec::iterator iter;
typedef std::vector< bool > bvec;
typedef bvec::iterator biter;
typedef common::vertex_score_heap< int > heap;

const int no_way_label = 1000000; // one million if no way

//================================================================================

// Outbound arc going from a vertex
struct elink
{
	elink()
		: target( 0 )
		, weight( 0 )
	{
	}

	elink( int t, int w )
		: target( t )
		, weight( w )
	{
	}

	int target; // head of the arc (vertex it leads to)
	int weight; // weight of the arc
};

//================================================================================

typedef std::vector< elink > elink_list;

//================================================================================

typedef std::vector< elink_list > graph_scheme;


//--------------------------------------------------------------------------------

std::istream& operator>> ( std::istream& input, elink& el )
{
	int t, w;
	char ch;
	input >> t >> ch;
	if ( ch == ',' )
	{
		input >> w;
		el = elink( t, w );
	}
	else
	{
		input.setstate( std::ios_base::failbit );
	}

	return input;
}

//--------------------------------------------------------------------------------

std::ostream& operator<< ( std::ostream& os, const elink& el )
{
	os << el.target << "," << el.weight;
	return os;
}


//================================================================================

class graph {

public:

	graph() {} 

	int vertex_count() const { return static_cast< int >( m_ve_map.size() ); }

	void append_vertex( int vertex, elink_list&& links )
	{
		PRECONDITION( vertices_are_continuous, vertex == m_ve_map.size() + 1 );
		m_ve_map.push_back( std::move( links ) );
	}

	const elink_list& links( int vertex ) const
	{
		return m_ve_map.at( internal_vertex_index( vertex ) );
	}

private:

	int internal_vertex_index( int vertex ) const { return vertex - 1; }

private: // fields

	graph_scheme m_ve_map; // vertex - edges map

};


//--------------------------------------------------------------------------------

void readline( std::istream& input, int& v, elink_list& links )
{
	std::string line;
	std::getline( input, line );
	std::istringstream buffer( line );
	buffer >> v; // vertex
	links.assign( std::istream_iterator< elink >( buffer ), std::istream_iterator< elink >() );
}

//--------------------------------------------------------------------------------

void load_graph( std::istream& input, graph& gr )
{
	while ( !input.eof() )
	{
		int vtx;
		elink_list links;
		
		readline( input, vtx, links );

		if ( input.good() )
		{
			gr.append_vertex( vtx, std::move( links ) );
		}

	} // while !input.eof
}


//--------------------------------------------------------------------------------

template < class T >
std::ostream& operator<< ( std::ostream& os, const std::vector< T >& v )
{
	std::copy( v.begin(), v.end(), std::ostream_iterator< T >( os, ", " ) );
	return os;
}


//--------------------------------------------------------------------------------

// Dijkstra algorithm implementaiton for shortest path
// gr - directed graph.
// s - the origin vertex. We calculate paths starting from here.
// a - vector where we write distances from the origin to each vertex
void find_shortest_paths( const graph& gr, heap::vertex_type s, vec& a )
{
	// by default we assume that there is no path to each vertex
	// +1 because we for simplicity enumerate vertices from 1 to n
	a.resize( gr.vertex_count() + 1, no_way_label );
	bvec x( a.size(), false ); // is the vertex already explored?
	heap h( a.size() ); // heap with vertices ordered by increasing distance from the origin

	int unprocessed_vertex_count = gr.vertex_count();

	// On each iteration we adsorb one more vertex into the set of explored vertices.
	// Therefore the algorithm cannot have more iterations then amount of vertices.
	CREATE_LOOP_VARIANT( unprocessed_vertices, unprocessed_vertex_count );

	h.push( s, 0 ); // we start from the origin and then will engage more an more new vertices
	
	while ( !h.empty() )
	{
		UPDATE_LOOP_VARIANT( unprocessed_vertices );
		--unprocessed_vertex_count;

		heap::vertex_type vertex;
		int distance;

		h.get_top( vertex, distance );
		h.pop(); // remove the adsorbed vertex

		a[ vertex ] = distance;
		x[ vertex ] = true;

		elink_list links = gr.links( vertex );

		for ( const elink& el : links )
		{

			if ( x.at( el.target ) ) // the node is already processed
				continue;

			int new_score = distance + el.weight;

			// The vertex is already in the heap and, 
			// as we just adsorbed another edge pointing to it, 
			// we need to recalculate its distance from the origin
			// because it might became closer
			if ( h.contains( el.target ) )
			{
				const int old_score = h.score( el.target );

				if ( new_score >= old_score )
					continue; // existing score is better

				h.remove( el.target );
			} // if contains target

			h.push( el.target, new_score );

		} // foreach elink

	} // while head is not empty
	
} // function find_shortes_path


//--------------------------------------------------------------------------------

int main()
{
	graph gr;
	load_graph( std::cin, gr );

	std::cout << "vertex.count=" << gr.vertex_count() << std::endl;

	// according to the problem definition:
	// the vertices to which we need to calculate the distance
	vec vertices_of_interest( { 7, 37, 59, 82, 99, 115, 133, 165, 188, 197 } );
	
	// vector with distances from the origin 1 to every node in the graph
	vec a;

	// Dijkstra shortest path algorithm
	find_shortest_paths( gr, 1, a );

	std::cout << "shortest paths:" << std::endl;

	for ( int vertex : vertices_of_interest )
	{
		std::cout << "a[" << vertex << "]=";

		if ( vertex <  gr.vertex_count() )
		{
			 std::cout << a.at( vertex ) << std::endl;
		}
		else
		{
			// The vertices listed in the problem definition must be represented in the array "a".
			// However we also have test data where these nodes might not be represented.
			// As we don't want the app to crash while printing the output for the test data we have to handle this situation nicely.
			std::cout << "n/a" << std::endl;
		}
	}

	std::cout << std::endl;
	
	return 0;
}
