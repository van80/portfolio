#include <functional>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>

#include <gtest/gtest.h>
#include <vertex_score_heap.hpp>

typedef common::vertex_score_heap< unsigned long > min_heap;
typedef common::vertex_score_heap< unsigned long, std::less<unsigned long> > max_heap;
typedef min_heap::vertex_type vertex_type;
typedef min_heap::score_type score_type;

namespace {

//--------------------------------------------------------------------------------

std::string to_string( const min_heap& h )
{
	typedef min_heap::vertex_score vertex_score;

	std::stringstream ss;
	std::transform( h.begin(), h.end(), std::ostream_iterator< int >( ss, ", " ), []( const vertex_score& vs ){ return vs.second; } );
	//std::clog << std::endl;
	return ss.str();
}

//--------------------------------------------------------------------------------

} // private namespace





//--------------------------------------------------------------------------------

/*
 * In this test we checks that newly created heap is a valid object
 */
TEST( VertexScoreHeapTest, Ctor )
{
	min_heap h( 1 );

	EXPECT_TRUE( h.empty() ); // newly created heap must be empty
	EXPECT_FALSE( h.full() ); // and not full

	EXPECT_EQ( 1, h.capacity() ); // the capacity corresponds to the argument of the .ctor
	EXPECT_EQ( 0, h.size() ); // size == 0 because the heap is empty

	EXPECT_FALSE( h.contains( 1 ) ); // it contains nothing
	EXPECT_EQ( 777, h.score( 1, 777 ) ); // score returns the error value provided by the client code
}

//--------------------------------------------------------------------------------

/*
 * Sanity check of how we handle items which are in the heap and which are not there
 */
TEST( VertexScoreHeapTest, PartiallyPopulated )
{
	min_heap h( 2 );
	h.push( 2, 47 ); // the heap is populated on half

	EXPECT_FALSE( h.empty() ); // therefore is not empty
	EXPECT_FALSE( h.full() ); // and is not full

	EXPECT_FALSE( h.contains( 1 ) ); // it does not contain things which we did not add to it
	EXPECT_TRUE( h.contains( 2 ) ); // but contains the vertex which really was added

	EXPECT_EQ( 999, h.score( 1, 999 ) ); // 999 - a placehodler for non-existent score
	EXPECT_EQ( 47, h.score( 2 ) ); // the score of the added vertex

	vertex_type top_vertex;
	score_type top_score;

	h.get_top( top_vertex, top_score ); // the only added entry is at the top

	EXPECT_EQ( 2, top_vertex );
	EXPECT_EQ( 47, top_score );
}

//--------------------------------------------------------------------------------

/*
 * We want to make sure that heap works as expected no matter in which order the items were added into it
 * This test checks the direct (increasing score) order.
 */
TEST( VertexScoreHeapTest, DirectOrderOfInsertion )
{
	min_heap h( 2 );
	h.push( 1, 71 );
	h.push( 2, 82 );

	EXPECT_FALSE( h.empty() ); // not empty
	EXPECT_TRUE( h.full() ); // capacity = 2, we added 2 items, the heap must be full

	// inserted vertices are in place
	EXPECT_TRUE( h.contains( 1 ) );
	EXPECT_TRUE( h.contains( 2 ) );

	// scores correspond to the values pushed
	EXPECT_EQ( 71, h.score( 1 ) );
	EXPECT_EQ( 82, h.score( 2 ) );

	vertex_type top_vertex;
	score_type top_score;

	// this is a min heap - therefore the top-most element is 
	// the one with smallest score, i.e. score == 71
	h.get_top( top_vertex, top_score );

	EXPECT_EQ( 1, top_vertex );
	EXPECT_EQ( 71, top_score );
}

//--------------------------------------------------------------------------------

/*
 * We want to make sure that heap works as expected no matter in which order the items were added into it
 * This test checks the inverted (decreasing score) order.
 */
TEST( VertexScoreHeapTest, InvertedOrderOfInsertion )
{
	min_heap h( 2 );
	h.push( 1, 119 );
	h.push( 2, 14 );

	EXPECT_FALSE( h.empty() ); // not empty
	EXPECT_TRUE( h.full() ); // capacity = 2, we added 2 items, the heap must be full

	// inserted vertices are in place
	EXPECT_TRUE( h.contains( 1 ) );
	EXPECT_TRUE( h.contains( 2 ) );

	// scores correspond to the values pushed
	EXPECT_EQ( 119, h.score( 1 ) );
	EXPECT_EQ( 14, h.score( 2 ) );

	vertex_type top_vertex;
	score_type top_score;

	// this is a min heap - therefore the top-most element is 
	// the one with smallest score, i.e. score == 71
	h.get_top( top_vertex, top_score );

	EXPECT_EQ( 2, top_vertex );
	EXPECT_EQ( 14, top_score );
}

//--------------------------------------------------------------------------------

/*
 * Less artificial case.
 * We populate the heap with random scores and among them we push one which is a-priory lower than the others.
 * The heap must make this element the top most
 */
TEST( VertexScoreHeapTest, MassiveInsertion )
{
	// our a-priory known smallest element
	const vertex_type expected_top_vertex = 100;
	const score_type expected_top_score = 33;

	min_heap h( 201 );

	for ( vertex_type v=1; v<expected_top_vertex; ++v )
	{
		h.push( v, rand() + expected_top_score + 1 ); // only higher scores
	}

	// this must become the top
	h.push( expected_top_vertex, expected_top_score );

	for ( vertex_type v=expected_top_vertex+1; v<=h.capacity(); ++v )
	{
		h.push( v, rand() + expected_top_score + 1 ); // only higher scores
	}


	EXPECT_FALSE( h.empty() ); // 201 items in the heap - it cannot be empty
	EXPECT_TRUE( h.full() ); // we populated the heap completely
	EXPECT_EQ( h.capacity(), h.size() ); // by definition of a full heap

	vertex_type top_vertex;
	score_type top_score;
	h.get_top( top_vertex, top_score );

	// now its the time to check our expectations regarding our top-most element
	EXPECT_EQ( expected_top_vertex, top_vertex );
	EXPECT_EQ( expected_top_score, top_score );

	// for completeness
	EXPECT_TRUE( h.contains( expected_top_vertex ) );
	EXPECT_EQ( expected_top_score, h.score( expected_top_vertex ) );
}

//--------------------------------------------------------------------------------

/*
 * Check how pop works in some simple case
 */
TEST( VertexScoreHeapTest, Pop )
{
	min_heap h( 2 );
	h.push( 2, 14 );
	h.push( 1, 88 );

	// this must effectively remove the entry (2, 14)
	// because it has the lowest score
	h.pop(); 

	// filled on half -> not empty and not full
	EXPECT_FALSE( h.empty() );
	EXPECT_FALSE( h.full() );

	EXPECT_EQ( 1, h.size() ); // only one element must survive
	EXPECT_EQ( 2, h.capacity() ); // but this should not affect capacity

	EXPECT_TRUE( h.contains( 1 ) ); // the element with bigger score is still there
	EXPECT_FALSE( h.contains( 2 ) ); // the element with the smallest score has been removed

	EXPECT_EQ( 88, h.score( 1 ) ); // the score corresponds to the value we pushed
	EXPECT_EQ( 666, h.score( 2, 666 ) ); // our fail safe function returns the error value for the removed element

	vertex_type top_vertex;
	score_type top_score;

	h.get_top( top_vertex, top_score );

	// the only remaining element should become the top-most
	EXPECT_EQ( 1, top_vertex );
	EXPECT_EQ( 88, top_score );
}

//--------------------------------------------------------------------------------

/*
 * Multiple pops also work correctly
 */
TEST( VertexScoreHeapTest, MultiPop )
{
	min_heap h( 4 );
	h.push( 1, 119 );
	h.push( 2, 14 );
	h.push( 3, 88 );
	h.push( 4, 729 ); // this element should survive

	h.pop(); // remove (1, 14)
	h.pop(); // remove (3, 88)
	h.pop(); // remove (1, 119)

	// we still have one element
	// therefore not empty and not full
	EXPECT_FALSE( h.empty() );
	EXPECT_FALSE( h.full() );

	EXPECT_EQ( 1, h.size() ); // single element is left in the heap
	EXPECT_EQ( 4, h.capacity() ); // the capacity is unaffected

	vertex_type top_vertex;
	score_type top_score;

	h.get_top( top_vertex, top_score );

	// the element with biggest capacity should survive
	EXPECT_EQ( 4, top_vertex );
	EXPECT_EQ( 729, top_score );
}

//--------------------------------------------------------------------------------

/*
 * Check if the heap really removes the items which we wanted to be removed
 */
TEST( VertexScoreHeapTest, Remove )
{
	min_heap h( 3 );
	h.push( 1, 14 ); // will be our top
	h.push( 2, 119 ); // to be removed
	h.push( 3, 87 );

	h.remove( 2 );

	// 2 of 3 items are still there
	EXPECT_FALSE( h.empty() );
	EXPECT_FALSE( h.full() );

	EXPECT_EQ( 2, h.size() ); // size was updated accordingly
	EXPECT_EQ( 3, h.capacity() ); // capacity is intact

	EXPECT_TRUE( h.contains( 1 ) ); // this must survive
	EXPECT_FALSE( h.contains( 2 ) ); // exactly this item should have been removed
	EXPECT_TRUE( h.contains( 3 ) ); // this also must survive

	EXPECT_EQ( 14, h.score( 1 ) ); // score of a survived item is intact
	EXPECT_EQ( 7654, h.score( 2, 7654 ) ); // fail-safe function returns error-value for the removed element
	EXPECT_EQ( 87, h.score( 3 ) ); // score of a survived item is intact

	vertex_type top_vertex;
	score_type top_score;

	// the smallest score is at the top
	h.get_top( top_vertex, top_score );

	EXPECT_EQ( 1, top_vertex );
	EXPECT_EQ( 14, top_score );
}

//--------------------------------------------------------------------------------

/*
 * This test checks that removal does not mess up the internal structure
 */
TEST( VertexScoreHeapTest, RemoveReInsert )
{
	min_heap h( 3 );
	h.push( 1, 14 ); // the top-most value
	h.push( 2, 119 ); // to be removed and re-inserted
	h.push( 3, 87 );

	h.remove( 2 );
	h.push( 2, 222 ); // re-inserte with new score

	// the heap is full
	EXPECT_FALSE( h.empty() );
	EXPECT_TRUE( h.full() );

	// the heap is full
	EXPECT_EQ( 3, h.size() );
	EXPECT_EQ( 3, h.capacity() );

	// all the items are in-place
	EXPECT_TRUE( h.contains( 1 ) );
	EXPECT_TRUE( h.contains( 2 ) );
	EXPECT_TRUE( h.contains( 3 ) );

	EXPECT_EQ( 14, h.score( 1 ) ); // unaffected
	EXPECT_EQ( 222, h.score( 2, 7654 ) ); // the updated score is in the heap
	EXPECT_EQ( 87, h.score( 3 ) ); // unaffected

	vertex_type top_vertex;
	score_type top_score;

	h.get_top( top_vertex, top_score );

	// the top is unaffected
	EXPECT_EQ( 1, top_vertex );
	EXPECT_EQ( 14, top_score );
}

//--------------------------------------------------------------------------------

/*
 * Scores don't have to be non-negative.
 */
TEST( VertexScoreHeapTest, NegativeScoresCreation )
{
	min_heap h( 3 );
	h.push( 1, -70 ); // the smallest score 
	h.push( 2, -9 );
	h.push( 3, -45 );

	// exactly what we inserted
	EXPECT_EQ( -70, h.score( 1 ) );
	EXPECT_EQ( -9, h.score( 2 ) );
	EXPECT_EQ( -45, h.score( 3 ) );

	vertex_type top_vertex;
	score_type top_score;

	h.get_top( top_vertex, top_score );

	// the smallest score is at the top
	EXPECT_EQ( 1, top_vertex );
	EXPECT_EQ( -70, top_score );
}

//--------------------------------------------------------------------------------

/*
 * Correct removal of items with negative scores
 */
TEST( VertexScoreHeapTest, NegativeScoresRemoval )
{
	min_heap h( 3 );
	h.push( 1, -70 ); // the top
	h.push( 2, -9 ); // to be removed
	h.push( 3, -45 );

	h.remove( 2 );

	EXPECT_EQ( -70, h.score( 1 ) ); // unaffected
	EXPECT_EQ( 334, h.score( 2, 334 ) ); // return the error score for the removed element
	EXPECT_EQ( -45, h.score( 3 ) ); // unaffected

	vertex_type top_vertex;
	score_type top_score;

	// the top is unaffected
	h.get_top( top_vertex, top_score );

	EXPECT_EQ( 1, top_vertex );
	EXPECT_EQ( -70, top_score );
}

//--------------------------------------------------------------------------------

/*
 * Regression white-box test for a bug found after some use.
 */
TEST( VertexScoreHeapRegressionTest, RemoveAndMaxheapify )
{
/*
 * 	I have found a bug in max_heapify function due to which hip variant could be violated in some circumstances.
 * 	The problem was due to use of incorrect starting point (begin) for calculation of the left child.
 */
	min_heap::vertex_score_cmp less;

	min_heap h( 7 );
	h.push( 1, 1 );
	h.push( 2, 200 );
	h.push( 3, 3 );
	h.push( 4, 198 );
	h.push( 5, 199 );
	h.push( 6, 4 );
	h.push( 7, 400 );

	h.remove( 2 );

	// check that internally the heap is really a heap
	EXPECT_TRUE( std::is_heap( h.begin(), h.end(), less ) ) << "min_heap.contents: " << to_string( h );
}
