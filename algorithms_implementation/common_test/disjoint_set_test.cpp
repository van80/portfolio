#include <functional>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>

#include <gtest/gtest.h>

#include <disjoint_set.hpp>

using common::disjoint_set;

//--------------------------------------------------------------------------------

TEST( DisjointSetTest, Ctor )
{
	disjoint_set djs( 2 );
	EXPECT_EQ( 2, djs.count_of_elements() );
	EXPECT_EQ( 2, djs.count_of_sets() );

	EXPECT_EQ( 0, djs.rank( 1 ) );
	EXPECT_EQ( 0, djs.rank( 2 ) );

	EXPECT_EQ( 1, djs.find( 1 ) );
	EXPECT_EQ( 2, djs.find( 2 ) );

	EXPECT_FALSE( djs.in_same_set( 1, 2 ) );
}

//--------------------------------------------------------------------------------

TEST( DisjointSetTest, CtorArbitrarySize )
{
	disjoint_set djs( 776 );
	EXPECT_EQ( 776, djs.count_of_elements() );
	EXPECT_EQ( 776, djs.count_of_sets() );
}

//--------------------------------------------------------------------------------

TEST( DisjointSetTest, Join )
{
	disjoint_set djs( 2 );

	djs.join( 1, 2 );

	EXPECT_EQ( 2, djs.count_of_elements() );
	EXPECT_EQ( 1, djs.count_of_sets() );

	// one of the elements should become a parent of the other
	EXPECT_TRUE( (1 == djs.find( 2 )) || (2 == djs.find( 1 )));
	EXPECT_TRUE( djs.in_same_set( 1, 2 ) );

	if (1 == djs.find( 2 )) // if 1 is lead
	{
		EXPECT_EQ( 1, djs.find( 1 ) );
		EXPECT_EQ( 1, djs.rank( 1 ) );
		EXPECT_EQ( 0, djs.rank( 2 ) );
	}
	else // 2 is lead
	{
		EXPECT_EQ( 2, djs.find( 2 ) );
		EXPECT_EQ( 1, djs.rank( 2 ) );
		EXPECT_EQ( 0, djs.rank( 1 ) );
	}
}

//--------------------------------------------------------------------------------

TEST( DisjointSetTest, LeftJoin )
{
	disjoint_set djs( 3 );

	djs.join( 1, 2 );
	djs.join( 2, 3 );

	EXPECT_EQ( 3, djs.count_of_elements() );
	EXPECT_EQ( 1, djs.count_of_sets() );

	EXPECT_TRUE( djs.in_same_set( 1, 3 ) );
	EXPECT_TRUE( djs.in_same_set( 2, 3 ) );

	EXPECT_NE( 3, djs.find( 3 ) );
	EXPECT_EQ( djs.find( 2 ), djs.find( 3 ) );
	EXPECT_EQ( 0, djs.rank( 3 ) );
	EXPECT_EQ( 1, djs.rank( djs.find( 3 ) ) );
}

//--------------------------------------------------------------------------------

TEST( DisjointSetTest, RightJoin )
{
	disjoint_set djs( 3 );

	djs.join( 2, 3 );
	djs.join( 1, 2 );

	EXPECT_EQ( 3, djs.count_of_elements() );
	EXPECT_EQ( 1, djs.count_of_sets() );

	EXPECT_TRUE( djs.in_same_set( 1, 2 ) );
	EXPECT_TRUE( djs.in_same_set( 1, 3 ) );

	EXPECT_NE( 1, djs.find( 1 ) );
	EXPECT_EQ( djs.find( 3 ), djs.find( 1 ) );
	EXPECT_EQ( 0, djs.rank( 1 ) );
	EXPECT_EQ( 1, djs.rank( djs.find( 1 ) ) );
}


