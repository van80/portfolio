\documentclass[a4paper]{article}

\title{C. Two Strings}
\author{}
\date{}

%		PACKAGES
\usepackage[T1]{fontenc}
\usepackage[fleqn]{amsmath} % fleqn - left align equations (instead of default centering
\usepackage{amssymb}
\usepackage{amsthm}
%\usepackage{array} % I don't remember why it came in pair iwht hyperref
%\usepackage{hyperref} % in order to write urls
\usepackage{clrscode3e}

%		AMSTHM ENTITIES
\newtheorem{myaxiom}{Axiom}
\newtheorem{mylemma}{Lemma}
\newtheorem{mytheorem}{Theorem}
\newtheorem{myrule}{Rule}
\newtheorem{mydef}{Definition}

%		CUSTOM COMMANDS
\newcommand{\iv}[1]{[#1]} % iverson bracket
\newcommand{\then}{\Rightarrow} % short implication sign
\newcommand{\tablealignment}{center}
\newcommand{\mytilde}{$\sim$} % nice tilda
\newcommand{\fallfac}[2]{{#1}^{\underline{#2}}}
\newcommand{\risefac}[2]{{#1}^{\overline{#2}}}
\newcommand{\concat}{\mathbin{@}}
\newcommand{\append}{\mathbin{++}}
\newcommand{\At}[2]{#1[#2]}
\newcommand{\Range}[3]{#1[#2 \twodots #3]}
\newcommand{\lsubseq}{\sqsubseteq}
\newcommand{\rsubseq}{\sqsupseteq}
\newcommand{\emptystr}{\lambda}
\newcommand{\strset}{\mathbb{S}}
\newcommand{\charset}{\mathbb{C}}
\newcommand{\interchangeable}{\iff}
\newcommand{\oneway}{\implies}

%		CUSTOM MATH OPERATORS
\newcommand{\lms}{\proc{Left-Subsequence-Lengths}}
\newcommand{\rms}{\proc{Right-Subsequence-Lengths}}
\newcommand{\gtms}{\proc{Longest-Subsequence}}
\newcommand{\CommentOne}{\>\>\>\>\>\>\Comment}
\newcommand{\MyString}[1]{``#1''}
\newcommand{\IH}{Inductive Hypothesis}
\newcommand{\BaseCase}{\item \emph{Base Case:} }
\newcommand{\InductiveStep}{\item \emph{Inductive Step:} }
\newcommand{\InductiveHypothesis}{\item \emph{Inductive Hypothesis:} }
\newcommand{\Termination}{\item \emph{Termination:} }
\newcommand{\ForwardProof}{\item $\Rightarrow$ }
\newcommand{\BackwardProof}{\item $\Leftarrow$ }
\newcommand{\BestK}{k_{best}}
\newcommand{\BestS}{s_\text{best}}
\newcommand{\LeftSubstring}{left-anchored substring}
\newcommand{\RightSubstring}{right-anchored substring}
\newcommand{\ProofByContradiction}{Proof (By Contradiction)}
\newcommand{\ProofByInduction}{Proof (By Induction)}
\newcommand{\ProofByContradictionAndInduction}{Proof (By Contradiction, By Induction)}
\newcommand{\RefA}[1]{(by A#1)} % reference on axiom
\newcommand{\RefL}[1]{(by L#1)} % reference on lemma
\newcommand{\RefT}[1]{(by T#1)} % reference on theorem
\newcommand{\RefIH}{(by the \IH)} % reference to inductive hypothesis
\newcommand{\RefWOP}{(by Well-Ordering Principle)} % reference to well-ordering principle

% HYPERREF package configuring
% https://en.wikibooks.org/wiki/LaTeX/Hyperlinks
%\hypersetup{
%	linktoc=none % no hyperrefs at TOC
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%									DOCUMENT
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
%\tableofcontents
%\clearpage
%
%
\section*{Problem}
%
You are given two strings $A$ and $B$. 
You have to remove the minimum possible number of \emph{consecutive} (standing one after another) characters from string $B$ in such a way that it becomes a subsequence of string $A$. 
It can happen that you will not need to remove any characters at all, or maybe you will have to remove all of the characters from $B$ and make it empty.

Subsequence of string $S$ is any such string that can be obtained by erasing zero or more characters \emph{(not necessarily consecutive)} from string $S$.
%
\paragraph{Input:}
The first line contains string $A$, and the second line --- string $B$. 
Both of these strings are nonempty and consist of lowercase letters of English alphabet. 
The length of each string is no bigger than $10^5$ characters.
%
\paragraph{Output:}
On the first line output a subsequence of string $A$, obtained from $B$ by erasing the minimum number of consecutive characters.
If the answer consists of zero characters, output <<->> (a minus sign).

\section*{Notation and Terminology} 

We will use the following notation in the text of the article:

\begin{enumerate}
	\item If we are talking about some abstract string we will refer it simply as $S$.
	\item If we want to be more specific we use names $A$ and $B$ in the same sense as in the problem definition.
	\item When we run out of names for strings we append any amount of prime <<$'$>> characters to the aforementioned names.
	\item In the section dedicated to the axiomatic we can declare strings using notation $s \in \strset$ or $p \in \strset$.
	\item Characters are declared in the following way $x \in \charset$ or $y \in \charset$.
	\item String of length $n$ is denoted as $S_n$.
	\item Individual characters are addressed via notation resembling many programming languages $A[k]$, $B[j]$.
	\item Range of characters starting at $k$ and ending before $j$,
		where $A[j]$ is not included, is denoted as $A_{k,j}$.
	\item For empty string we use symbol $\emptystr$ or, alternatively, empty range $S_{k,k}$.
	\item Notation $B \lsubseq A$ means that string $B$ is a subsequence of string $A$.
		They can possibly match exactly.
\end{enumerate}

Sometimes we are talking about \LeftSubstring{} and \RightSubstring{}.
These notions stand for substrings $S_{0,k}$ and $S_{j,n}$ of string $S_n$ respectively,
where $k$ and $j$ are arbitrary integers $0 \leqslant k,j \leqslant n$.

As our frequent task will be to find the longest possible subsequence of $A_n$ 
such that it is a substring of $B_m$ delimited by range $B_{k,j}$
 we will often use the shortcut 
\emph{``the longest $B_{k,j}$ of $B_m$ for $A_n$''} or even 
\emph{``the longest $B_{k,j}$ for $A_n$''} instead of this phrase.

All strings are of finite size.


%
%
%
%
%
\section*{Solution}

The head-on solution would be to try removing all possible ranges of indices in $B$ 
and find the longest resulting string which would be subsequence of $A$.
Performance of this algorithms can be roughly assessed as follows:

Traversal of all possible ranges in $B$ requires $\binom{m}{2} = O(m^2)$ time 
because we in essence need to check all possible pairs of indices denoting ends of the range to be removed.
For each range we need to check whether the resulting string is subsequence of $A$ which takes $O(n)$ time.
As a result we have $O(n m^2)$ time.

\subsection*{Can we do better?}

It turns out that we can solve the problem in linear time.

The problem definition can be rephrased in the following way:
We need to find the longest \LeftSubstring{} $B_{0,l}$ and the longest \RightSubstring{} $B_{r,m}$ of $B_m$
such that $B_{0,l}B_{r,m} \lsubseq A$ and $0 \leqslant l \leqslant r \leqslant m$.

The general idea behind our solution is to split string $A$ in two in all possible places.
For each split $A_{0,k}$, $A_{k,n}$ of $A_n$ we find two substrings of $B_m$:
\begin{enumerate}
  \item $B_{0,l}$ --- the longest possible \LeftSubstring{} of $B_m$ such that $B_{0,l} \lsubseq A_{0,k}$;
  \item $B_{r,m}$ --- the longest possible \RightSubstring{} of $B_m$ such that $B_{r,m} \lsubseq A_{k,n}$.
\end{enumerate}
The best split with maximum amount of matching characters between $B_{0,l}B_{r,m}$
and $A_{0,k}A_{k,n}$ is memorized.
Finally we combine $B_{0,l}$ and $B_{r,m}$ of the best split which gives us 
the longest possible subsequence of $A$ obtained from  $B$ by deletion of the shortest consecutive range of characters.

The implementation needs to take care that $B_{0,l}$ and $B_{r,m}$ do not overlap.
Such situation is possible for instance if we split string $A=$\MyString{barsabarsa} exactly in the middle 
and then match both halves with string $B=$\MyString{barsa}.
The cases like this are not difficult to detect because they will yield strings $B_{0,l}$ and $B_{r,m}$ 
which, being combined, produce a string longer than $B_m$.

\subsection*{Is the solution really optimal?}

Imagine that you have found some subsequence $B'$ of $A_n$ obtained from $B_m$ by removal of some consecutive range of characters
and that this string $B'$ is longer than $B_{0,l}B_{r,m}$.
Split the string $A_n$ in such a way that all the characters in $B'$ before the removed range 
form a subsequence of the left substring of $A_n$ 
and all the characters in $B'$ after the removed range
form a subsequence of the right substring of $A_n$.
If there is no characters removed from $B$ at all then we can make the left substrings empty
with all the statements above still valid.
Such a split of $A$ is indeed always possible 
and as our algorithm traverses all splits of $A$ then it must have been found.
You can find more rigorous proofs below.

%
%
%
%
%
\section*{Algorithm}

The algorithms encompasses three functions \lms{}, \rms{} and \gtms{}.
The first one finds lengths of longest $B_{0,j}$ for all possible splits of $A_n$,
the second one finds analogical $B_{j,m}$ values,
and finally \gtms{} finds the solution for the entire problem.

%Before giving the implementation we need to prove a couple of lemmas.


\begin{codebox}
\Procname{$\lms(A,B,L)$}
\li \Comment [Precondition] $\attrib{L}{length} \isequal \attrib{A}{length} + 1$
\li	$n \gets \attrib{A}{length}$
\li	$m \gets \attrib{B}{length}$
\li	\Comment $j$ is the biggest possible value such that $B_{0,j}$ is a subsequence of $A_{0,k}$.
\li	\Comment As range $A_{0,0}$ is empty, only empty string $B_{0,0}$ can be its subsequence.
\li $j \gets 0$
\li $L[0] \gets j$
\li \For $k \gets 0$ \To $n-1$
	\Do
\li		\If $j < m$ and $A[k] \isequal B[ j ]$
		\Then
\li			$j \gets j+1$
		\End
\li		$L[k+1] \gets j$
	\End % for k=0 to n-1
\end{codebox}



\begin{codebox}
\Procname{$\rms(A,B,R)$}
\li \Comment [Precondition] $\attrib{R}{length} \isequal \attrib{A}{length} + 1$
\li	$n \gets \attrib{A}{length}$
\li	$m \gets \attrib{B}{length}$
\li	\Comment $j$ is the smallest possible value such that $B_{j,m}$ is a subsequence of $A_{k,n}$.
\li	\Comment As range $A_{n,n}$ is empty, only empty string $B_{m,m}$ can be its subsequence.
\li $j \gets m$
\li $R[n] \gets m-j$
\li \For $k \gets n-1$ \Downto $0$
	\Do
\li		\If $j > 0$ and $A[k] \isequal B[j-1]$
		\Then
\li			$j \gets j - 1$
		\End
\li		$R[k] \gets m - j$
	\End % for k=n downto 1
\end{codebox}



\begin{codebox}
\Procname{$\gtms(A,B)$}

\li	$n \gets \attrib{A}{length}$
\li	$m \gets \attrib{B}{length}$

\li \Comment Let $L$ - array of size $n + 1$
\li $\lms( A, B , L )$
\li \Comment Let $R$ - array of size $n + 1$
\li $\rms( A, B, R )$

\li	\Comment $\BestS$ is amount of matching characters for the best split in range $[0 \twodots k - 1]$
\li $\BestS \gets L[0] + R[0]$
\li	\Comment $\BestK$ is index of the best split of $A$ in range $[0 \twodots k-1]$
\li $\BestK \gets 0$

\li \For $k \gets 1$ \To $n$
	\Do
\li		\Comment At each iteration we check the split 
\li		\Comment made right before the $k$-th character in string $A$
\li		$s \gets L[k] + R[k]$	
\li		\If $s > \BestS$
		\Then
\li			$\BestS \gets s$
\li			$\BestK \gets k$ 
		\End % if s > s_best
	\End % for k=1 to n

\li \If $\BestS \geqslant m$
	\Then
\li		\Return $B$
\li	\Else
\li		\Return $B_{0,L[\BestK]} B_{m - R[\BestK], m}$
	\End

\end{codebox}



%
%
%
%
%
\section*{Correctness}


We will prove correctness of our algorithms in order of their appearance.

The author did his best to narrate his proofs in intuitively clear way so that reader had no need to rigorously check them.
Nevertheless the key statements are supplied with references to appropriated axioms \RefA{1}, lemmas \RefL{2} and theorems \RefT{3}.
All the axioms and lemmas can be found in the next section.

\begin{mytheorem}[$\lms$ Loop Invariant]
\item	$\forall k': 0 \leqslant k' \leqslant k \then L[k']$ is length of the longest possible $B_{0,j'}$ 
such that it is a subsequence of $A_{0,k'}$
\end{mytheorem}
\begin{proof}[\ProofByInduction]
	\BaseCase $k=0$, $L[0]=j=0$ because \RefA{\ref{empty_string_no_subsequences}} 
		only empty string $B_{0,0}$ can be subsequence of empty string $A_{0,0}$
	\InductiveStep 
		Given the \IH{} holds for all iterations before $k$ and $k<n$
		we can have three possible cases:
		\begin{itemize}
			\item if $j=m$ then the whole $B_m$ is a subsequence of $A_{0,k+1}$.
				We set $L[k+1]$ to $m$ and don't increase $j$ because $B_{0,m}$ is already 
				the longest substring of $B_m$.
			\item if $j<m$ and $B[j] = A[k]$ then \RefL{\ref{longest_subseq_ctor_r1}} 
				$B_{0,j+1}$ is the longest subsequence of $A_{0,k+1}$.
				As length of $B_{0,j+1}$ is $j+1$ we increase $j$ by 1 and assign it to $L[k+1]$
				which as a result must contain length of the longest $B_{0,j'}$ for $A_{0,k+1}$.
			\item if $j<m$ and $B[j] \neq A[k]$ then \RefL{\ref{longest_subseq_ctor_r2}} 
				$B_{0,j}$ is the longest subsequence of $A_{0,k+1}$.
				We leave $j$ unchanged and assign it to $L[k+1]$.
		\end{itemize}
		In all cases given above, after the increment of $k$, 
		both $L[k]$ and $j$ store length of the longest possible $B_{0,j'}$ for $A_{0,k}$.
		Lower indices of $L$ remain unchanged.
		Therefore the \IH{} still holds.
	\Termination $k = n$, 
	$\forall k': 0 \leqslant k' \leqslant n \then L[k']$ is length of the longest possible $B_{0,j'}$ for $A_{0,k'}$
	The last value in $L$ contains length of the longest possible \LeftSubstring{} of $B_m$ for the whole string $A_n$.
\end{proof}


\begin{mytheorem}[$\rms$ Loop Invariant]
\item	$\forall k': k < k' \leqslant n \then R[k']$ is length of the longest possible $B_{j',m}$ 
such that it is a subsequence of $A_{k',n}$
\end{mytheorem}
\begin{proof}[\ProofByInduction]
	\BaseCase $k=n$, $j=n$, $R[n]=0$ because \RefA{\ref{empty_string_no_subsequences}} 
		only empty string $B_{m,m}$ can be subsequence of empty string $A_{n,n}$
	\InductiveStep 
		Given the inductive hypothesis holds for all $k': k'>k$ and given that $k \geqslant 0$
		we can have three possible cases:
		\begin{itemize}
			\item if $j=0$ then the whole $B_m$ is a subsequence of $A_{k,n}$.
				We set $L[k]$ to $0$ and don't decrease $j$ because $B_{0,m}$ is already 
				the longest substring of $B_m$.
			\item if $j > 0$ and $B[j-1] = A[k]$ then \RefL{\ref{longest_subseq_ctor_l1}} 
				$B_{j-1,m}$ is the longest subsequence of $A_{k,n}$.
				We decrease $j$ by one and assign the length of $B_{j,m}$, which is $m-j$, to $L[k]$.
			\item if $j > 0$ and $B[j-1] \neq A[k]$ then \RefL{\ref{longest_subseq_ctor_l2}} 
				$B_{j,m}$ is the longest subsequence of $A_{k,n}$.
				We leave $j$ unchanged and assign $m-j$, the length of $B_{j,m}$, to $L[k]$.
		\end{itemize}
		In all cases given above, after the decrement of $k$, 
		both $L[k+1]$ and $j$ store length of the longest possible $B_{j',m}$ for $A_{k+1,n}$.
		Higher indices of $L$ remain unchanged.
		Therefore the \IH{} still holds.
	\Termination $k = -1$, 
	$\forall k': k < 0 \leqslant k' \leqslant n \then L[k']$ is length of the longest possible $B_{j',m}$ for $A_{k',n}$
	The first value in $L$ contains length of the longest possible \RightSubstring{} of $B_m$ for the whole string $A_n$.
\end{proof}


%\subsection*{\gtms}

\begin{mytheorem}[$\gtms$ Loop Invariant] 
	\label{gtms_loop_invariant}
	There are two properties maintained by the Loop Invariant.
	\begin{enumerate}
		\item Variable $\BestK$ contains the best splitting point of $A$ in range $[0 \twodots k-1]$.
		\item Variable $\BestS$ contains amount of matching characters for the best split of $A$ in range $[0 \twodots k-1]$.
	\end{enumerate}
\end{mytheorem}
\begin{proof}[\ProofByInduction]
	\BaseCase $k=1$, $\BestK$ and $\BestS$ correspond to the split done before position $0$ in string $A$.
		This is the best split for range $[0 \twodots 0]$
	\InductiveStep Given the \IH{} holds for all iterations up to $k$,
		$\BestK$ and $\BestS$ are position and amount of matching characters of the best split in range $[0 \twodots k-1]$.
		If amount of matching characters of the split before position $k$ is better then $\BestS$ we update $\BestK$ and $\BestS$ accordingly.
		Otherwise we keep them unchanged.
		After the increment of $k$ the \IH{} still holds.
	\Termination $k=n+1$, $\BestK$ and $\BestS$ contain position and amount of matching characters for the best split in the range $[0 \twodots n]$.
\end{proof}

\begin{mytheorem}
	There is no $0 \leqslant l' \leqslant r' \leqslant m$ such that $B'=B_{0,l'}B_{r',m}$, $B' \lsubseq A$ 
	and length of $B'$ is greater than length of the string $B_{0,l}B_{r,m}$ returned by $\gtms$.
\end{mytheorem}
\begin{proof}[\ProofByContradiction]
	Assume that such a string $B'$ exists.
	Then \RefL{\ref{split_b_implies_split_a}} there must be a corresponding split of $A$ into $A_{0,k'}$ and $A_{k',n}$
	such that $B_{0,l'} \lsubseq A_{0,k'}$ and $B_{r',m} \lsubseq A_{k',n}$.
	However we know that we have traversed all possible splits of $A_n$ in $\gtms$ and that the best matching subsequence $B_{0,l}B_{r,m}$
	has been found and returned by \gtms{} \RefT{\ref{gtms_loop_invariant}}.
	Therefore our assumption contradicts \gtms{} Loop Invariant and cannot be truth.
\end{proof}

%
%
%
%
%
\section*{Axiomatic}

This section exists just for the sake of bookkeeping of the assumptions we made.
We are not trying to develop any formalism here ---
all the proofs in this article are essentially informal.

\begin{myaxiom}%[Empty String]
\label{empty_string}
There exists empty string: $\emptystr \in \strset$
\end{myaxiom}

\begin{myaxiom}%[Empty String Has No Subsequences]
\label{empty_string_no_subsequences}
Only empty string can be subsequence of empty string.
\begin{equation*}
	\forall s \in \strset: s \lsubseq \emptystr \then s = \emptystr 
\end{equation*}
\end{myaxiom}

%\begin{myaxiom}[Neutrality]
%\label{empty_string_neutrality}
%Concatenation of any string with empty string $\emptystr$ (at any end) yields the original string.
%\begin{equation*}
%	\forall s \in \strset: s\emptystr{} = s = \emptystr{}s
%\end{equation*}
%\end{myaxiom}

\begin{myaxiom}[Reflexivity]
\label{reflexivity}
Any string is a subsequence of itself.
\begin{equation*}
	\forall s \in \strset: s \lsubseq s 
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Add Same Right Character]
\label{add_same_right}
Same character can be added to the right end of any string and its subsequence.
\begin{equation*}
	\forall p,s \in \strset: \forall x \in \charset: p \lsubseq s \oneway px \lsubseq sx 
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Remove Same Right Character]
\label{remove_same_right}
Same character can be removed from the right end of any string and its subsequence.
\begin{equation*}
	\forall p,s \in \strset: \forall x \in \charset: px \lsubseq sx \oneway p \lsubseq s 
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Add Same Left Character]
\label{add_same_left}
Same character can be added to the left end of any string and its subsequence.
\begin{equation*}
	\forall p,s \in \strset: \forall x \in \charset: p \lsubseq s \oneway xp \lsubseq xs
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Remove Same Left Character]
\label{remove_same_left}
Same character can be removed from the left end of any string and its subsequence.
\begin{equation*}
	\forall p,s \in \strset: \forall x \in \charset: xp \lsubseq xs \oneway p \lsubseq s 
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Add Unmatched Right Character]
\label{add_unmatched_right}
It is possible to add any character to the right end of any string.
\begin{equation*}
	\forall p,s \in \strset: \forall x \in \charset: p \lsubseq s \oneway p \lsubseq sx 
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Remove Unmatched Right Character]
\label{remove_unmatched_right}
It is possible to remove any unmatched character from the right end of any string.
\begin{gather*}
	\forall s \in \strset: \forall x \in \charset: \emptystr \lsubseq sx \oneway \emptystr \lsubseq s \\
	\forall p,s \in \strset: \forall x \neq y \in \charset: py \lsubseq sx \oneway py \lsubseq s 
\end{gather*}
\end{myaxiom}

\begin{myaxiom}[Add Unmatched Left Character]
\label{add_unmatched_left}
It is possible to add any character to the left end of any string
\begin{equation*}
	\forall p,s \in \strset: \forall x \in \charset: p \lsubseq s \oneway p \lsubseq xs
\end{equation*}
\end{myaxiom}

\begin{myaxiom}[Remove Unmatched Left Character]
\label{remove_unmatched_left}
It is possible to remove any unmatched character from the left end of any string.
\begin{gather*}
	\forall s \in \strset: \forall x \in \charset: \emptystr \lsubseq xs \oneway \emptystr \lsubseq s  \\
	\forall p,s \in \strset: \forall x \neq y \in \charset: yp \lsubseq xs \oneway yp \lsubseq s 
\end{gather*}
\end{myaxiom}

\subsection*{Auxiliary theorems}

Now it is the time to infer some interesting facts about strings and subsequences.
In order to do this we will leverage the axioms given above.

%\subsection*{Lemmas}

%\begin{mylemma}
%Assume that empty string $B_0$ is the longest possible \LeftSubstring{} of string $B_l$ 
%such that it is a subsequence of non-empty string $A_k$.
%Then $B_0$ is the longest possible subsequence of $A_{k-1}$.
%\end{mylemma}
%\begin{proof}[\ProofByContradiction]
%Assume that there exists $B_{l'}$ such that $l' > 0$ and $B_{l'}$ is a subsequence of $A_{k-1}$.
%Then by Axiom \ref{add_unmatched_right} $B_{l'}$ is a subsequence of $A_k$ as well.
%This contradicts the assumption that $B_0$ is the longest possible subsequence of $A_k$ and therefore impossible.
%
%Assume that $B_0$ is not subsequence of $A_{k-1}$ at all.
%According to Axiom \ref{remove_unmatched_right} $B_0$ is a subsequence of $A_{k-1}$. 
%Contradiction!
%\end{proof}
%
%\begin{mylemma}
%Assume that $B_l$ is the longest possible non-empty \LeftSubstring{} of $B_m$  such that it is a subsequence of $A_k$ and that $B[l-1] \neq A[k-1]$
%Then $B_{l}$ is the longest possible subsequence of $A_{k-1}$.
%\end{mylemma}
%\begin{proof}[\ProofByContradiction]
%Assume that there exists $B_{l'}$ such that $l' > l$ and $B_{l'}$ is a subsequence of $A_{k-1}$.
%Then in accordance with Axiom \ref{add_unmatched_right} $B_{l'}$ is also a subsequence of $A_k$.
%This contradicts the assumption that $B_l$ is the longest possible subsequence of $A_k$ and therefore impossible.
%
%Assume that $B_l$ is not a subsequence of $A_{k-1}$ at all.
%This goes in contradiction with Axiom \ref{remove_unmatched_right} and therefore is impossible.
%\end{proof}
%
%\begin{mylemma}
%Assume that $B_l$ is a longest possible subsequence of $A_k$, it is non-empty and $B[l-1] = A[k-1]$
%Then $B_{k-1}$ is the longest possible subsequence of $A_{k-1}$.
%\end{mylemma}
%\begin{proof}[\ProofByContradiction]
%Assume that there exists $B_{l'}$ such that $l' > l-1$ and $B_{l'}$ is a subsequence of $A_{k-1}$.
%Then appending $B[k]=A[n]$ to $B_{l'}$ we obtain string $B_{l'+1}$ 
%which (by Axiom \ref{add_same_right}) is a subsequence of $A_k$
%and which is longer than $B_l$ because $l' > l-1 \then l' + 1 > l$.
%This contradicts the assumption that $B_l$ is the longest possible subsequence of $A_k$ and therefore impossible.
%
%Assumption that $B_{l-1}$ is not a subsequence of $A_{k-1}$ is in contradiction with Axiom \ref{remove_same_right} and therefore is incorrect.
%\end{proof}

\begin{mylemma}
\label{longest_subseq_ctor_r1}
Given $0 \leqslant j <m $, $0 \leqslant k <n$, where $m$ is length of $B_m$ and $n$ is length of $A_n$.
If $B_{0,j}$ is the longest possible \LeftSubstring{} of $B_m$ for $A_{0,k}$ and $B[j] = A[k]$ 
then $B_{0,j+1}$ is the longest possible \LeftSubstring{} of $B_m$ for $A_{0,k+1}$.
\end{mylemma}
\begin{proof}[\ProofByContradiction]
Assume that there is a string $B_{0,j'}$ which is longer than $B_{0,j+1}$ and it is a subsequence of $A_{0,k+1}$.
\begin{enumerate}
	\item If $B[j'-1]=A[k]$ then \RefA{\ref{remove_same_right}} $B_{0,j'-1}$ is a subsequence of $A_{0,k}$ 
		and it is longer than $B_{0,j}$ because $j' > j+1 \then j'-1 > j$.
	\item If $B[j'-1] \neq A[k]$ then \RefA{\ref{remove_unmatched_right}} $B_{0,j'}$ is a subsequence of $A_{0,k}$ 
		and it is longer than $B_{0,j}$ because $j' > j + 1 > j$.
\end{enumerate}
Both of theses cases contradict the assumption that $B_{0,j}$ is the longest possible \LeftSubstring{} of $B_m$ being subsequence of $A_{0,k}$.

Assume that length of the longest possible $B_{0,j'}$ for $A_{0,k+1}$ is less than $j+1$.
The string obtained by appending $A[k]$ to $B_{0,j}$ is a subsequence of $A_{0,k+1}$ \RefA{\ref{add_same_right}}
and it is longer than $B_{0,j'}$ by our assumption that $j' < j+1$.
Then we have a contradiction with statement that $B_{0,j'}$ is the longest possible \LeftSubstring{} of $B_m$ for $A_{0,k+1}$.
\end{proof}

\begin{mylemma}
\label{longest_subseq_ctor_r2}
Given $0 \leqslant j < m$, $0 \leqslant k < n$, where $m$ is length of $B_m$ and $n$ is length of $A_n$.
If $B_{0,j}$ is the longest possible \LeftSubstring{} of $B_m$ for $A_{0,k}$ and $B[j] \neq A[k]$ 
then $B_{0,j}$ is the longest possible \LeftSubstring{} of $B_m$ for $A_{0,k+1}$.
\end{mylemma}
\begin{proof}[\ProofByContradiction]
Assume that there is a string $B_{0,j'}$ which is longer than $B_{0,j}$ and which is a subsequence of $A_{0,k+1}$.
Let's consider three possible cases of how it might look like:
\begin{enumerate}
	\item $j'=j+1$. As $B[j'-1]=B[j] \neq A[k]$ then
		$B_{0,j'} \lsubseq A_{0,k}$ \RefA{\ref{remove_unmatched_right}} and it is longer than $B_{0,j}$.
	\item $j'>j+1$ and $B[j'-1]=A[k]$. Then \RefA{\ref{remove_same_right}} we can obtain string $B_{0,j'-1} \lsubseq A_{0,k}$ 
		which is longer than $B_{0,j}$ because $j' > j+1 \then j'-1 > j$.
	\item $j'>j+1$ and $B[j'-1] \neq A[k]$. Then \RefA{\ref{remove_unmatched_right}} $B_{0,j'} \lsubseq A_{0,k}$ 
		and it is longer than $B_{0,j}$ because $j' > j+1 > j$.
\end{enumerate}
In all three cases given above we come to contradiction with the assumption that
$B_{0,j}$ is the longest possible \LeftSubstring{} of $B_m$ being subsequence of $A_{0,k}$.

Assume that length of the longest possible $B_{0,j'}$ for $A_{0,k+1}$ is less than $j+1$.
We know \RefA{\ref{add_unmatched_right}} that $B_{0,j}$ is a subsequence of $A_{0,k+1}$
and it is longer than $B_{0,j'}$.
This contradicts the assumption that $B_{0,j'}$ is the longest possible \LeftSubstring{} of $B_m$ for $A_{0,k+1}$.
\end{proof}

\begin{mylemma}
\label{longest_subseq_ctor_l1}
Given $0 < j \leqslant m$, $0 < k \leqslant n$, where $m$ is length of $B_m$ and $n$ is length of $A_n$.
If $B_{j,m}$ is the longest possible \RightSubstring{} of $B_m$ for $A_{k,n}$ and $B[j-1] = A[k-1]$ 
then $B_{j-1,m}$ is the longest possible \RightSubstring{} of $B_m$ for $A_{k-1,n}$.
\end{mylemma}
\begin{proof}[\ProofByContradiction]
Assume that there is a string $B_{j',m}$ which is longer than $B_{j-1,m}$ and which is a subsequence of $A_{k-1,n}$.
\begin{enumerate}
	\item  If $B[j'] = A[k-1]$ then \RefA{\ref{remove_same_left}} $B_{j'+1,m}$ is a subsequence of $A_{k,n}$ 
		and it is longer than $B_{j,m}$ because $m-j' > m-(j-1) \then m-(j'+1) > m-j$.
	\item  If $B[j'] \neq A[k-1]$ then \RefA{\ref{remove_unmatched_left}} $B_{j',m}$ is a subsequence of $A_{k,n}$ 
		and it is longer than $B_{j,m}$ because $m-j' > m-(j-1) > m-j$.
\end{enumerate}
Both of these cases contradict the assumption that $B_{j,m}$ is the longest possible \RightSubstring{} of $B_m$ for $A_{k,n}$.

Assume that length of the longest possible $B_{j',m}$ for $A_{k-1,n}$ is less than $m-(j-1)$.
The string obtained by adding $A[k-1]$ to the left end of $B_{j,m}$ is a subsequence of $A_{k-1,n}$ \RefA{\ref{add_same_left}}
and it is longer than $B_{j',m}$ by our assumption that $m-j' < m-(j-1)$.
Then we have a contradiction with statement that $B_{j',m}$ is the longest possible subsequence of $A_{k,n}$.
\end{proof}

\begin{mylemma}
\label{longest_subseq_ctor_l2}
Given $0 < j \leqslant m$, $0 < k \leqslant n$, where $m$ is length of $B_m$ and $n$ is length of $A_n$.
If $B_{j,m}$ is the longest possible \RightSubstring{} of $B_m$ for $A_{k,n}$ and $B[j-1] \neq A[k-1]$ 
then $B_{j,m}$ is the longest possible \RightSubstring{} of $B_m$ for $A_{k-1,n}$.
\end{mylemma}
\begin{proof}[\ProofByContradiction]
Assume that there is a string $B_{j',m}$ which is longer than $B_{j,m}$ and which is a subsequence of $A_{k-1,n}$.
Let's consider three possible cases of how it might look like:
\begin{enumerate}
	\item $j'=j-1$. As $B[j']=B[j-1] \neq A[k-1]$ then
		$B_{j',m} \lsubseq A_{k,n}$ \RefA{\ref{remove_unmatched_left}} and it is longer than $B_{j,m}$.
	\item $j'<j-1$ and $B[j']=A[k-1]$. Then \RefA{\ref{remove_same_left}} we can obtain string $B_{j'+1,m} \lsubseq A_{k,n}$ 
		which is longer than $B_{j,m}$ because $j' < j-1 \then j'+1 < j \then m-(j'+1) > m-j$.
	\item $j'<j-1$ and $B[j'] \neq A[k-1]$. Then \RefA{\ref{remove_unmatched_left}} $B_{j',m} \lsubseq A_{k,n}$ 
		and it is longer than $B_{j,m}$ because $j' < j-1 < j \then m-j' > m-(j-1) > m-j $.
\end{enumerate}
In all three cases given above we come to contradiction with the assumption that
$B_{j,m}$ is the longest possible \RightSubstring{} of $B_m$ being subsequence of $A_{k,n}$.

Assume that length of the longest possible $B_{j',m}$ for $A_{k-1,n}$ is less than $m-j$.
We know \RefA{\ref{add_unmatched_left}} that $B_{j,m}$ is a subsequence of $A_{k-1,n}$
and it is longer than $B_{j',m}$.
This contradicts the assumption that $B_{j',m}$ is the longest possible \RightSubstring{} of $B_m$ for $A_{k-1,n}$.
\end{proof}

%\begin{mylemma}
%\label{split_a_implies_split_b}
%$\forall k:$ If $A_n=A_{0,k}A_{k,n}$ and $B_m \lsubseq A_n$ then there exist $j$ 
%such that $B_m=B_{0,j}B_{j,m}$, $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,n}$ .
%\end{mylemma}
%\begin{proof}[\ProofByInduction]
%\BaseCase k=0. Let $j=0$. Then $B_{0,0} \lsubseq A_{0,0}$ \RefA{\ref{reflexivity}} and $B_{0,m} \lsubseq A_{0,n}$ \RefIH{}.  
%\InductiveStep Assume \IH holds up to some $k$, i.e. there exists $j$ such that $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,n}$.
%	\begin{enumerate}
%		\item If $B[j] = A[k]$ then $B_{0,j+1} \lsubseq A_{0,k+1}$ \RefA{\ref{add_same_right}} and  $B_{j+1,m} \lsubseq A_{k+1,n}$
%			\RefA{\ref{remove_same_left}}.
%		\item If $B[j] \neq A[k]$ then $B_{0,j} \lsubseq A_{0,k+1}$ \RefA{\ref{add_unmatched_right}} and  $B_{j,m} \lsubseq A_{k+1,n}$
%			\RefA{\ref{remove_unmatched_left}}.
%	\end{enumerate}
%\end{proof}

%\begin{mylemma}
%\label{remove_all_unmatching_characters}
%If we have a character $x \in \charset$ and string $S_n$ such that.
%\end{mylemma}

\begin{mylemma}
\label{first_matching_character_exists}
Given non-empty string $B_m$ is a subsequence of string $A_n$ and $B[0] \neq A[0]$.
There exists $k$ such that $B_m \lsubseq A_{k,n}$ and $B[0] = A[k]$
\end{mylemma}
\begin{proof}[\ProofByContradictionAndInduction]
%Assume that such $k$ does not exist which means that $\forall k': 0 \leqslant k' < n \then B[0] !=
Assume the such $k$ does not exist.
Then we can remove any amount of characters from the left end of string $A_n$ \RefA{\ref{remove_unmatched_left}} 
and after every removal the resulting string $A'$ is one character shorter than before,
its first character still differs from the first character of $B_m$, i.e. $B[0] \neq A'[0]$ (by the hypothesis of this Lemma)
and \RefA{\ref{remove_unmatched_left}} relation $B_m \lsubseq A'$ still holds.
As string $A_n$ has finite length then sooner or later it becomes empty while
$B_m$ still remains to be its subsequence.
However we know \RefA{\ref{empty_string_no_subsequences}} that only empty string can be subsequence of empty string
which means that $B_m$ must also be emtpy.
We have reached contradiction with assumption that $B_m$ is not empty.
\end{proof}

%\begin{mylemma}
%\label{split_b_implies_split_a}
%$\forall j:$ If $B_m=B_{0,j}B_{j,m}$ and $B_m \lsubseq A_n$ then there exist $k$
%such that $A_n=A_{0,k}A_{k,n}$, $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,n}$.
%\end{mylemma}
%\begin{proof}[\ProofByContradiction]
%Assume that this is not true and for some $j$ there is no $k$ such that $A_n=A_{0,k}A_{k,n}$, $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,n}$
%	\begin{enumerate}
%		\item Assume that $j=0$. 
%			Then by choosing $k=0$ we get $B_{0,0} \lsubseq A_{0,0}$ \RefA{\ref{reflexivity}} and $B_{0,m} \lsubseq A_{0,n}$ \RefIH{}
%			This contradicts the assumption that such $k$ does not exist.
%		\item Assume that $j>0$ and it is the minimum such value \RefWOP. 
%			This means that \IH still holds for $j'=j-1$ and some $k'$, i.e.
%%			$A_n=A_{0,k'}A_{k',n}$, $B_{0,j'} \lsubseq A_{0,k'}$ and $B_{j',m} \lsubseq A_{k',n}$
%%			However we know that \RefL{\ref{split_a_implies_split_b}} for any $k, k'<k<m$ there must be $j$ such that.
%%			But in this case \RefL{\ref{split_a_implies_split_b}} there must be some $k, k'<k<m$ such that
%	\end{enumerate}
%\end{proof}

\begin{mylemma}
\label{split_b_implies_split_a}
$\forall j:$ If $B_m=B_{0,j}B_{j,m}$ and $B_m \lsubseq A_n$ then there exist $k$
such that $A_n=A_{0,k}A_{k,n}$, $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,n}$ .
\end{mylemma}
\begin{proof}[\ProofByInduction]
\BaseCase j=0. Let $k=0$. Then $B_{0,0} \lsubseq A_{0,0}$ \RefA{\ref{reflexivity}} 
	and $B_{0,m} \lsubseq A_{0,n}$ \RefIH{} because $B_m \lsubseq A_n$.  
\InductiveStep Assume that \IH{} holds up to some $j$, i.e. $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,n}$.
	\begin{enumerate}
		\item If $B[j] = A[k]$ then $B_{0,j+1} \lsubseq A_{0,k+1}$ \RefA{\ref{add_same_right}} and  $B_{j+1,m} \lsubseq A_{k+1,n}$
			\RefA{\ref{remove_same_left}}.
		\item If $B[j] \neq A[k]$ then \RefL{\ref{first_matching_character_exists}} we can remove unmatched characters from the left end of $A_{k,n}$
			and add them the right end of $A_{0,k}$ \RefA{\ref{add_unmatched_right}},
			which is equivalent of incrementing $k$, until $B[j]=A[k]$, $B_{0,j} \lsubseq A_{0,k}$ and $B_{j,m} \lsubseq A_{k,m}$.
			As $B[j] = A[k]$ then $B_{0,j+1} \lsubseq A_{0,k+1}$ \RefA{\ref{add_same_right}} and  $B_{j+1,m} \lsubseq A_{k+1,n}$
			\RefA{\ref{remove_same_left}}.
	\end{enumerate}
\end{proof}



%
%
%
%
%
\section*{Analysis}
%
It is not difficult to see that both $\lms$,
$\rms$ functions as well as the loop in $\gtms$ have running time of $\Theta(n)$.
One might argue that running time of $\lms$ and $\rms$ must be $O(n+m)$. 
This is indeed incorrect because we know for sure 
that loop will terminate as soon as it has traversed all $n$ characters in string $A$;
and it cannot traverse more than $n$ characters in string $B$ 
because the counter is incremented only in case of match between characters in both strings 
and there cannot be more than $n$ matches.

Therefore the whole algorithms has running time of $\Theta(n)$.

%%
%%
%%
%%
%%
%\section*{Earlier Attempts}
%%
%We will act in spirit of Name and Conquer strategy by naming the entities we are going to use and then assembling the solution out of them.
%
%\paragraph{Name.} This is the list of entities we are going to use.
%{
%%\renewcommand{\descriptionlabel}[1]{$#1$ -}
%\newcommand{\dsep}{-}
%%\newcommand{\Range}[2]{#1[#2]}
%\begin{description}
%	\item[ $S_n$ \dsep ] String of length $n$.
%			The indices vary from 0 to $n-1$.
%	\item[ $\At{S}{k}$ ] Symbol at index $k$ of string $S$.
%	\item[ $\Range{S}{k_b}{k_e}$ \dsep ] Range of symbols in string $S$. 
%			The range begins at position $k_b$ and ends right before position $k_e$.
%			That is $S[k_e]$ is not included into the range.
%	\item[ $A_n$ or $A$ \dsep ] Input string against which we are going to match the pattern.
%			Its length will be referred as $n$.
%	\item[ $B_m$ or $B$ \dsep ] The pattern we need to reduce in order to find the best bossible match. 
%			Its length will be referred as $m$.
%	\item[ $C_d$ or $C$ \dsep ] The best matching sequence - 
%			the pattern string $B_m$ from which we have removed the shortest possible consequtive sequence of symbols.
%	\item[ $X \concat Y$ \dsep ] String concatenation operator.
%%	\item[ $f(S, P) : \Range{Q}{k_b}{k_e}$ \dsep ] Function returning the longest common subsequence of strings $S$ and $P$. 
%%			The subsequence is continuous within $P$ - i.e. it is a substring of $P$.
%%			The indices are given in the coordinate system of string $P$, 
%%			where $k_b$ first matching index in $P$, $k_e$ index following the last matching symbol in $P$. 
%%	\item[ $h(S, \Range{P}{k_b}{k_e}) : \Range{Q}{k_b}{j}$ \dsep ] Longest leading subsequence of strings $S$ and $\Range{P}{k_b}{k_e}$. 
%%			The subsequence is continuous within $P$ and it terminates exactly at position $m - 1$.
%%	\item[ $t(S, P_m) : \Range{Q}{k_b}{m}$ \dsep ] Function returning the longest trailing subsequence of strings $S$ and $P_m$. 
%%			The subsequence is continuous within $P$ and it terminates exactly at position $m - 1$.
%\end{description}
%}
%

%
%		BIBLIOGRAPHY
%\begin{thebibliography}{9}
%	\bibitem{geb} Author's Name, year, \emph{Book Title}, Publisher.
%\end{thebibliography}
%
\end{document}
