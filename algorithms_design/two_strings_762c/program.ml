let print_result s = 
	match s with
	| "" ->
		Printf.printf "-\n";
	| _ ->
		Printf.printf "%s\n" s;
;;


let left_subsequence_lengths a b =

	let n = String.length a in
	let m = String.length b in
	let l = Array.make (n+1) 0 in
	let j = ref 0 in
	begin

	l.(0) <- !j;

	for k=0 to (n-1) do

		if (!j < m) && ((String.get a k) == (String.get b !j))  then
			begin
			j := !j + 1;
			end;

		l.(k+1) <- !j; (* now l[!js] contains amount of matches between b and a[0..js-1] *)

	done;

	l
	end
;;

let right_subsequence_lengths a b =
	let n = String.length a in
	let m = String.length b in
	let r = Array.make (n+1) 0 in
	let j = ref m in
	begin

	r.(n) <- (m - !j);

	for k = (n-1) downto 0 do

		if (!j > 0) && ((String.get a k ) == (String.get b (!j-1)))  then
			begin
			j := !j - 1;
			end;

		r.(k) <- (m - !j);

	done;

	r;
	end
;;

let longest_subsequence a b =
	let n = String.length a in
	let m = String.length b in
	let l = left_subsequence_lengths a b in
	let r = right_subsequence_lengths a b in
	let s_best = ref (l.(0) + r.(0)) in
	let k_best = ref 0 in
	begin
	for k = 1 to n do
		let s = l.(k) + r.(k) in
		if s > !s_best then
			begin
			s_best := s;
			k_best := k;
			end;
	done;

	if !s_best >= m
		then b
		else (String.sub b 0 l.(!k_best)) ^ (String.sub b (m - r.(!k_best)) r.(!k_best))
	end
;;

let read_input () = 
	Scanf.bscanf Scanf.Scanning.stdin " %s %s " (fun a b -> print_result (longest_subsequence a b))
;;

read_input ()
