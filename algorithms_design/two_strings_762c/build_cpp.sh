#!/usr/bin/env bash

this_dir="$(dirname $0)"
build_dir="$this_dir/build"

rm -rf "$build_dir" \
	&& mkdir "$build_dir" \
	&& cd "$build_dir" \
	&& cmake .. \
	&& make
