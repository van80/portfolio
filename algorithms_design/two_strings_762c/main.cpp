#include <iostream>
#include <string>
#include <vector>

typedef std::string str;
typedef std::vector< int > vec;


vec left_subsequence_lengths( const str& a, const str& b )
{
	vec l( a.size() + 1 );

	vec::iterator l_it = l.begin();
	*l_it = 0;

	str::size_type j = 0;

	for ( str::const_iterator k_it = a.cbegin(); k_it != a.cend(); ++k_it )
	{
		// k_it points to A[k]
		++l_it; // j_it points to L[k+1]

		if ( (j < b.size()) && (*k_it == b.at(j)) )
		{
			++j;
		}

		*l_it = j;
	}

	return l;
}


vec right_subsequence_lengths( const str& a, const str& b )
{
	vec r( a.size() + 1 );

	vec::reverse_iterator r_it = r.rbegin();
	*r_it = 0; // R[k+1] = 0

	str::size_type j = b.size();

	for ( str::const_reverse_iterator k_it = a.crbegin(); k_it != a.crend(); ++k_it )
	{
		// k_it points to A[k]
		++r_it; // j_it points to R[k]

		if ( (j > 0) && (*k_it == b.at(j-1)) )
		{
			--j;
		}

		*r_it = b.size() - j;
	}

	return r;
}


std::string longest_subsequence( const str& a, const str& b )
{
	vec::size_type n = a.size() + 1;

	vec l = left_subsequence_lengths( a, b );
	vec r = right_subsequence_lengths( a, b );
	vec::size_type k_best = 0;
	int s_best = l.at( 0 ) + r.at( 0 );

	for ( str::size_type k = 1; k < n; ++k )
	{
		int s = l.at( k ) + r.at( k );
		if ( s > s_best )
		{
			s_best = s;
			k_best = k;
		}
	}

	if ( s_best >= b.size() )
	{
		return b;
	}
	else
	{
		str result;
		result.reserve( s_best );
		result.assign( b.begin(), std::next( b.begin(), l.at( k_best ) ) );
		result.insert( result.end(), std::next( b.begin(), b.size() - r.at( k_best ) ), b.end() );
		return result;
	}
}


int main()
{
	std::string a, b;

	std::cin >> a;
	std::cin >> b;

	std::string s = longest_subsequence( a, b );

	if ( s.empty() )
	{
		std::cout << "-" << std::endl;
	}
	else
	{
		std::cout << s << std::endl;
	}

	return 0;
}
