cmake_minimum_required (VERSION 2.8.11)

project( two_strings )

set( CMAKE_CXX_FLAGS "-std=c++11" )
set( CMAKE_CXX_STANDARD 11 )
set( CMAKE_CXX_STANDARD_REQUIRED ON )

configure_file( input_1.txt input_1.txt COPYONLY )
configure_file( input_2.txt input_2.txt COPYONLY )
configure_file( input_3.txt input_3.txt COPYONLY )
configure_file( input_5.txt input_5.txt COPYONLY )
configure_file( input_30.txt input_30.txt COPYONLY )
configure_file( README README COPYONLY )

add_executable( two_strings main.cpp )
