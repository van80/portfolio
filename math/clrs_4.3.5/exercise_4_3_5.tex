\documentclass[a4paper]{article}

\title{Exercise 4.3-5}
\author{}
\date{}

%		PACKAGES
\usepackage[T1]{fontenc}
\usepackage[fleqn]{amsmath} % fleqn - left align equations (instead of default centering
\usepackage{amssymb}
\usepackage{amsthm}
%\usepackage{array} % I don't remember why it came in pair iwht hyperref
\usepackage{hyperref} % in order to write urls

%		AMSTHM ENTITIES
\newtheorem{myaxiom}{Axiom}
\newtheorem{mylemma}{Lemma}
\newtheorem{mytheorem}{Theorem}
\newtheorem{myrule}{Rule}

%		CUSTOM COMMANDS
\newcommand{\iv}[1]{[#1]} % iverson bracket
\newcommand{\then}{\Rightarrow} % short implication sign
\newcommand{\tablealignment}{center}
\newcommand{\ensuretablespacing}{\renewcommand{\arraystretch}{1.7}}
\newcommand{\mytilde}{$\sim$} % nice tilda
\newcommand{\fallfac}[2]{{#1}^{\underline{#2}}}
\newcommand{\risefac}[2]{{#1}^{\overline{#2}}}

% HYPERREF package configuring
% https://en.wikibooks.org/wiki/LaTeX/Hyperlinks
\hypersetup{
	linktoc=none % no hyperrefs at TOC
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%									DOCUMENT
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
%\tableofcontents
%\clearpage
%
\subsection*{The Problem (Merge Sort Analysis)}
%
Prove that 
$$T(n) = T\left(\left\lceil\frac{n}{2}\right\rceil\right) + T\left(\left\lfloor\frac{n}{2}\right\rfloor\right) + \Theta(n) = \Theta(n \log n).$$
That is there exist three constants $c_1, c_2 > 0$ and $n_0 \geqslant 0$ such that 
$$\forall n \geqslant n_0: c_1 n \lg n \leqslant T(n) \leqslant c_2 n \lg n.$$
As we know 
$$T(n) \in \Theta(n \log n) \iff T(n) \in O(n \log n) \land T(n) \in \Omega(n \log n).$$
This fact allows us proving the statement by parts.

%
\subsection*{Part 1. $T(n) \in O(n \log n)$}
%
We will make a stronger assumption $T(n) \leqslant cn \lg(n - 1)$.
It is also convenient to rewrite $\Theta(n)$ as $an + b$ where $a > 0$.
%
\begin{align*}
	T(n) &= T\left(\left\lceil\frac{n}{2}\right\rceil\right) + T\left(\left\lfloor\frac{n}{2}\right\rfloor\right) + an + b \quad & c>0,\, a>0 \\
		&\leqslant c\left\lceil \frac{n}{2}\right\rceil \lg\left(\left\lceil \frac{n}{2} \right\rceil - 1\right) 
					+ c\left\lfloor \frac{n}{2}\right\rfloor \lg\left(\left\lfloor \frac{n}{2} \right\rfloor - 1\right)
					+ an + b \quad & c>0,\, a>0 \\
		&\leqslant c\left\lceil \frac{n}{2}\right\rceil \lg\left(\frac{n+1}{2} - 1\right) 
					+ c\left\lfloor \frac{n}{2}\right\rfloor \lg\left(\frac{n+1}{2} - 1\right)
					+ an + b \quad & c>0,\, a>0 \\
		&\leqslant cn \lg\left(\frac{n-1}{2}\right) + an + b \quad & c>0,\, a>0 \\
		&\leqslant cn \lg(n-1) - cn + an + b \quad & c>0,\, a>0 \\
		&\leqslant cn \lg(n-1) - \bigl((c-a)n - b\bigr) \quad & c>0,\, a>0 \\
		&\leqslant cn \lg(n-1), \quad & c > a > 0, n \geqslant \frac{b}{c-a}  \\
\end{align*}
%
As for the base case the situation is the following:
\begin{\tablealignment}
\ensuretablespacing
\begin{tabular}{cllc}
	$n$	&	$T(n)$					& $cn \lg (n-1)$			& $c$ 							\\
	\hline
	1	&	1						& $\text{\emph{undefined}}$	& \emph{n/a} 					\\
	2	&	$2T(1) + 2 = 4$			& $0$						& \emph{n/a} 					\\
	3	&	$T(2) + T(1) + 3 = 8$	& $3c$						& $c \geqslant \frac{8}{3}$		\\
	4	&	$2T(2) + 4 = 12$		& $4 \lg(3) c$				& $c \geqslant \frac{3}{\lg 3}$	\\ 
	5	&	$T(3) + T(2) + 5 = 17$	& $10c$						& $c \geqslant \frac{17}{10}$	\\
\end{tabular}
\end{\tablealignment}

%
Apparently we cannot use $n=1$ or $n=2$ as base case because the bounding function becomes undefined or zero respectively.
Therefore we need to have 3 base cases $n \in [3..5]$. 
All the values above 5 (i.e. 6 and higher) after multiple division by two will fall in this range.
From the table above we can see that it is sufficient to pick $c=3$ in order to make $T(n) \leqslant cn \lg (n - 1)$.
As we have also proven the inductive step we can be sure now that given $c \geqslant \max(3, a)$ 
the inequality holds for all $n \geqslant \max\left(3, \frac{b}{c-a}\right)$. 

In the last step we recall that the function $\lg n$ is increasing. 
Therefore we can state that $\lg (n-1) < \lg n$ 
and by transitivity $T(n) \leqslant cn \lg n$.
This finalizes our proof that $T(n) = O(n \log n)$.
%
%
%

\subsection*{Part 2. $T(n) \in \Omega(n \log n)$}
%
Here the inductive hypothesis will be slightly different $T(n) \geqslant cn \lg(n + 1)$.
%
\begin{align*}
	T(n) &= T\left(\left\lceil\frac{n}{2}\right\rceil\right) + T\left(\left\lfloor\frac{n}{2}\right\rfloor\right) + an + b \quad & c>0,\, a>0 \\
		&\geqslant c\left\lceil \frac{n}{2}\right\rceil \lg\left(\left\lceil \frac{n}{2} \right\rceil + 1\right) 
					+ c\left\lfloor \frac{n}{2}\right\rfloor \lg\left(\left\lfloor \frac{n}{2} \right\rfloor + 1\right)
					+ an + b \quad & c>0,\, a>0 \\
		&\geqslant c\left\lceil \frac{n}{2}\right\rceil \lg\left(\frac{n-1}{2} + 1\right) 
					+ c\left\lfloor \frac{n}{2}\right\rfloor \lg\left(\frac{n-1}{2} + 1\right)
					+ an + b \quad & c>0,\, a>0 \\
		&\geqslant cn \lg\left(\frac{n+1}{2}\right) + an + b \quad & c>0,\, a>0 \\
		&\geqslant cn \lg(n+1) - cn + an + b \quad & c>0,\, a>0 \\
		&\geqslant cn \lg(n+1) + \bigl((a-c)n + b\bigr) \quad & c>0,\, a>0 \\
		&\geqslant cn \lg(n+1), \quad & a > c > 0,\, n \geqslant \frac{b}{c-a} \\
\end{align*}
%
The base case here is easier than in the previous part: $T(1)=1$ and $c\cdot 1 \lg 2 = c$.
That is $c = 1$ is sufficient.
We need only to adjust it with the constraints imposed by the inductive step, i.e. $0 < c \leqslant \min(1, a)$ 
in order to make the inequality hold for all $n \geqslant \max\left(1, \frac{b}{c-a}\right)$.

Finally we again rely on properties of logarithmic function (that it is increasing) and transitivity of inequality relation
in order to prove that $$T(n) \geqslant cn \lg(n+1) \geqslant cn \lg n$$ which means that $T(n)$ is indeed $\Omega(n \log n)$.
%
%
%
%

%
%
%
%		BIBLIOGRAPHY
%\begin{thebibliography}{9}
%	\bibitem{geb} Author's Name, year, \emph{Book Title}, Publisher.
%\end{thebibliography}
%
\end{document}
