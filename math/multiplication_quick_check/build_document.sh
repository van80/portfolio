#!/usr/bin/env bash

project_name="multiplication_quick_check"

if [ -z "$project_name" ]; then
	echo "$(basename $0): Variable \"project_name\" is undefined" >&1
	exit 1;
fi

pdflatex -shell-escape "$project_name" && 
	pdflatex -shell-escape "$project_name"
