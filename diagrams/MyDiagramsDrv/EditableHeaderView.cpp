#include <EditableHeaderView.hpp>

//--------------------------------------------------------------------------------

EditableHeaderView::EditableHeaderView( Qt::Orientation orientation, QWidget* parent )
	: BaseClass( orientation, parent )
	, m_textEdit( new QLineEdit( this ) )
	, m_sectionBeingEdited( -1 )
{
	m_textEdit->setVisible( false );
	m_textEdit->installEventFilter( this );
}

//--------------------------------------------------------------------------------

void EditableHeaderView::mouseDoubleClickEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::LeftButton )
	{
		QRect currentSectionRect;

		const int logical = logicalIndexAt( event->pos() );

		if ( logical >= 0 )
		{
			const int width = viewport()->width();
			const int height = viewport()->height();

			if (orientation() == Qt::Horizontal) {
				currentSectionRect.setRect(sectionViewportPosition(logical), 0, sectionSize(logical), height);
			} else {
				currentSectionRect.setRect(0, sectionViewportPosition(logical), width, sectionSize(logical));
			}

			//currentSectionRect.translate(offset);
			
			QString headerText = model()->headerData( logical, orientation(), Qt::EditRole ).toString();

			m_textEdit->setText( headerText );

			m_sectionBeingEdited = logical;
			
			m_textEdit->setGeometry( currentSectionRect );
			m_textEdit->setVisible( true );
			m_textEdit->setFocus();
		}

	}
	else
	{
		BaseClass::mouseDoubleClickEvent( event );
	}
}

//--------------------------------------------------------------------------------

void EditableHeaderView::commitData()
{
	model()->setHeaderData( m_sectionBeingEdited, orientation(), m_textEdit->text(), Qt::EditRole );
}

//--------------------------------------------------------------------------------

void EditableHeaderView::closeEditor()
{
	m_sectionBeingEdited = -1;
	m_textEdit->setVisible( false );
}

//--------------------------------------------------------------------------------

void EditableHeaderView::commitAndCloseEditor()
{
	commitData();
	closeEditor();
}

//--------------------------------------------------------------------------------

bool EditableHeaderView::eventFilter( QObject* object, QEvent* event )
{
	if ( object != m_textEdit )
	{
		return BaseClass::eventFilter( object, event );
	}

    if (event->type() == QEvent::KeyPress) 
	{
		switch (static_cast<QKeyEvent *>(event)->key()) 
		{
			case Qt::Key_Enter:
			case Qt::Key_Return:
				if (!m_textEdit->hasAcceptableInput())
					return false;
				commitAndCloseEditor();
				//QMetaObject::invokeMethod(this, "commitAndCloseEditor", Qt::QueuedConnection);
				return true;
			case Qt::Key_Escape:
				// don't commit data
				closeEditor();
				return true;
			default:
				return false;
		}
    } 
	else if (event->type() == QEvent::FocusOut || (event->type() == QEvent::Hide && m_textEdit->isWindow())) 
	{
        //the Hide event will take care of he editors that are in fact complete dialogs
		if (!m_textEdit->isActiveWindow() || (QApplication::focusWidget() != m_textEdit)) 
		{
			QWidget *w = QApplication::focusWidget();
			while (w) { // don't worry about focus changes internally in the editor
				if (w == m_textEdit)
					return false;
				w = w->parentWidget();
			}

			commitData();
			closeEditor();
		}
    } 
	else if (event->type() == QEvent::ShortcutOverride) 
	{
		if (static_cast<QKeyEvent*>(event)->key() == Qt::Key_Escape) {
			event->accept();
			return true;
		}
	}
    return false;
}
