#pragma once

#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>

namespace Ui {
	class TableEditor;
} // namespace Ui


class TableEditor : public QWidget
{
Q_OBJECT

public: // types

	typedef QWidget BaseClass;

public: // methods

	TableEditor( QWidget* parent = nullptr );

	~TableEditor();

	QAbstractItemModel* model() const { return m_model; }

	void setModel( QAbstractItemModel* model );

private Q_SLOTS:

	void onCustomContextMenuRequestedAtTableView( const QPoint& pos );

	void onCustomContextMenuRequestedAtVerticalHeader( const QPoint& pos );

	void onCustomContextMenuRequestedAtHorizontalHeader( const QPoint& pos );

	void insertRowBeforeCurrent();

	void insertRowAfterCurrent();

	void removeCurrentRow();

	void insertColumnBeforeCurrent();

	void insertColumnAfterCurrent();

	void removeCurrentColumn();

private:
	
	Q_DISABLE_COPY( TableEditor );

	void setRowToValue( int row, int value );

	void setColumnToValue( int column, int value );

private: // fields

	Ui::TableEditor* ui;
	QAbstractItemModel* m_model;
	QMenu* m_menu;
    QAction* m_actionInsertRowBeforeCurrent;
    QAction* m_actionInsertRowAfterCurrent;
    QAction* m_actionRemoveCurrentRow;
    QAction* m_actionInsertColumnBeforeCurrent;
    QAction* m_actionInsertColumnAfterCurrent;
    QAction* m_actionRemoveCurrentColumn;
	int m_currentRow;
	int m_currentColumn;

}; // class TableEditor
