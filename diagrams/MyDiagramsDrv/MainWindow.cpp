#include <MainWindow.hpp>
#include <ui_MainWindow.h>

#include <BarDiagram.hpp>
#include <QtSvg/QSvgGenerator>

#include <TableEditor.hpp>

namespace {

const QString lastSaveAsSvgKey = "last_save_as_svg_path";
const QString lastSaveAsPngKey = "last_save_as_png_path";
const QString tableEditorGeometryKey = "table_editor_geometry";

} // private namespace

//--------------------------------------------------------------------------------

MainWindow::MainWindow( QWidget* parent )
: BaseClass( parent )
, ui( new Ui::MainWindow() )
, m_model( new QStandardItemModel( this ) )
, m_tableEditor( new TableEditor( this ) )
{
	ui->setupUi( this );

	setupModel( m_model );
	ui->barDiagram->setModel( m_model );

	//m_tableEditor->setWindowFlags( Qt::Popup );
	m_tableEditor->setWindowFlags( Qt::Window );
	m_tableEditor->setWindowTitle( tr( "Table Editor" ) );
	m_tableEditor->setModel( m_model );
	m_tableEditor->installEventFilter( this );

	connect( ui->pushButtonMoveLeft, &QPushButton::clicked, ui->barDiagram, &BarDiagram::shiftLeft );
	connect( ui->pushButtonMoveRight, &QPushButton::clicked, ui->barDiagram, &BarDiagram::shiftRight );
	connect( ui->pushButtonMoveUp, &QPushButton::clicked, ui->barDiagram, &BarDiagram::shiftUp );
	connect( ui->pushButtonMoveDown, &QPushButton::clicked, ui->barDiagram, &BarDiagram::shiftDown );
	connect( ui->pushButtonCenter, &QPushButton::clicked, ui->barDiagram, &BarDiagram::gotoCenter );
	connect( ui->pushButtonZoomIn, &QPushButton::clicked, ui->barDiagram, &BarDiagram::zoomIn );
	connect( ui->pushButtonZoomOut, &QPushButton::clicked, ui->barDiagram, &BarDiagram::zoomOut );

	connect( ui->pushButtonOpenEditor, &QPushButton::clicked, this, &MainWindow::openEditor );
	connect( ui->actionSaveAsSvg, &QAction::triggered, this, &MainWindow::onSaveAsSvg );
	connect( ui->actionSaveAsPng, &QAction::triggered, this, &MainWindow::onSaveAsPng );
}

//--------------------------------------------------------------------------------

MainWindow::~MainWindow()
{
	delete ui;
	ui = nullptr;
}

//----------------------------------------------------------------------

void MainWindow::setupModel( QStandardItemModel* model )
{
	model->clear();

	QStringList columnLabels, rowLabels;
	columnLabels << "2011" << "2012" << "2013" << "2014";
	rowLabels << "Series #1" << "Series #2" << "Series #3";

	typedef QList< QList< int > > DataList;
	typedef QList< int > IntList;
	DataList data;
	data.push_back( IntList() << 4 << 5 << 4 << 3 );
	data.push_back( IntList() << 5 << 4 << 7 << 7 );
	data.push_back( IntList() << 13 << 22 << 35 << 41 );

	for ( int row = 0; row < rowLabels.size(); ++row )
	{
		QList< QStandardItem* > rowItems;

		IntList rowData = data.at( row );

		for ( int column = 0; column < columnLabels.size(); ++column )
		{
			QStandardItem* item = new QStandardItem();
			item->setData( rowData.at( column ), Qt::DisplayRole );
			item->setData( rowData.at( column ), Qt::EditRole );
			rowItems.push_back( item );
		}

		model->appendRow( rowItems );
	}

	model->setHorizontalHeaderLabels( columnLabels );
	model->setVerticalHeaderLabels( rowLabels );

	for ( int row = 0; row < rowLabels.size(); ++row )
	{
		model->verticalHeaderItem( row )->setEditable( true );
	}

	for ( int column = 0; column < rowLabels.size(); ++column )
	{
		model->horizontalHeaderItem( column )->setEditable( true );
	}
}

//----------------------------------------------------------------------

void MainWindow::openEditor()
{
	QSettings settings( QApplication::instance()->organizationName() );
	QByteArray serializedGeometry = settings.value( tableEditorGeometryKey ).toByteArray();
	m_tableEditor->restoreGeometry( serializedGeometry );
	m_tableEditor->show();
}

//----------------------------------------------------------------------

void MainWindow::onSaveAsSvg()
{
	QSettings settings( QApplication::instance()->organizationName() );
	QString oldDestPath = settings.value( lastSaveAsSvgKey, QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation ) ).toString();
	

	QString destPath = QFileDialog::getSaveFileName( this, tr( "Save as SVG" ), 
						oldDestPath, tr( "SVG files (*.svg)" ) );

	if ( destPath.isEmpty() )
		return;

	settings.setValue( lastSaveAsSvgKey, destPath );

	QSvgGenerator svgGenerator;
	svgGenerator.setFileName( destPath );
	ui->barDiagram->writeTo( &svgGenerator );
}

//----------------------------------------------------------------------

void MainWindow::onSaveAsPng()
{
	QSettings settings( QApplication::instance()->organizationName() );
	QString oldDestPath = settings.value( lastSaveAsPngKey, QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation ) ).toString();

	QString destPath = QFileDialog::getSaveFileName( this, tr( "Save as PNG" ), 
						oldDestPath, tr( "PNG files (*.png)" ) );

	if ( destPath.isEmpty() )
		return;

	settings.setValue( lastSaveAsPngKey, destPath );

	QPixmap pixmap = ui->barDiagram->toPixmap();
	pixmap.save( destPath, "PNG" );
}

//----------------------------------------------------------------------

bool MainWindow::eventFilter( QObject* obj, QEvent* event )
{
	if ( (obj == m_tableEditor) && (event->type() == QEvent::Close) )
	{
		QSettings settings( QApplication::instance()->organizationName() );
		settings.setValue( tableEditorGeometryKey, m_tableEditor->saveGeometry() );
	}

	return BaseClass::eventFilter( obj, event );
}
