#pragma once

#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>

//================================================================================

namespace Ui {
	class MainWindow;
}

class TableEditor;

//================================================================================

class MainWindow : public QMainWindow {

public: //types

	typedef QMainWindow BaseClass;

public: // methods

	MainWindow( QWidget* parent = nullptr );

	~MainWindow();

	void setupModel( QStandardItemModel* model );

protected:

	bool eventFilter( QObject* obj, QEvent* event ) override;

private Q_SLOTS:

	void openEditor();

	void onSaveAsSvg();

	void onSaveAsPng();

private: // fields

	Ui::MainWindow* ui;
	QStandardItemModel* m_model;
	TableEditor* m_tableEditor;

}; //class MainWindow
