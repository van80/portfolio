#include <TableEditor.hpp>
#include <ui_TableEditor.h>

#include <EditableHeaderView.hpp>
#include <BarDiagramConstants.hpp>

namespace {

const int defaultSectionSize = 70;
const int maxRowCount = BarDiagramConstants::themeColorCount;

//--------------------------------------------------------------------------------

QWidget* contextMenuWidget( QObject* sender )
{
	if ( sender->inherits( "QAbstractScrollArea" ) )
	{
		QAbstractScrollArea* scrollArea = qobject_cast< QAbstractScrollArea* >( sender );
		return scrollArea->viewport();
	}

	return qobject_cast< QWidget* >( sender );
}

//--------------------------------------------------------------------------------

QIcon iconFromTheme( const QString& iconThemeName )
{
	QIcon icon;
	if (QIcon::hasThemeIcon(iconThemeName)) {
		icon = QIcon::fromTheme(iconThemeName);
	} else {
		icon.addFile(QStringLiteral(""), QSize(), QIcon::Normal, QIcon::Off);
	}
	return icon;
}

//================================================================================

//! A delegate preventing negative numbers as input
class MyDelegate : public QStyledItemDelegate
{
public: // types

	typedef QStyledItemDelegate BaseClass;

public: // methods

	//--------------------------------------------------------------------------------

	MyDelegate( QObject* parent = nullptr )
		: BaseClass( parent )
	{
	}

	//--------------------------------------------------------------------------------

	QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override
	{
		QWidget* editor = BaseClass::createEditor( parent, option, index );

		QSpinBox* spinBox = qobject_cast< QSpinBox* >( editor );
		if ( spinBox != nullptr )
		{
			spinBox->setMinimum( 0 );
			return spinBox;
		}

		QDoubleSpinBox* dspinBox = qobject_cast< QDoubleSpinBox* >( editor );
		if ( dspinBox != nullptr )
		{
			dspinBox->setMinimum( 0 );
			return dspinBox;
		}

		return editor;
	}
}; // class MyDelegate

} // private namespace



//--------------------------------------------------------------------------------

TableEditor::TableEditor( QWidget* parent )
	: BaseClass( parent )
	, ui( new Ui::TableEditor() )
	, m_model( nullptr )
	, m_menu( new QMenu( this ) )
    , m_actionInsertRowBeforeCurrent( nullptr )
    , m_actionInsertRowAfterCurrent( nullptr )
    , m_actionRemoveCurrentRow( nullptr )
    , m_actionInsertColumnBeforeCurrent( nullptr )
    , m_actionInsertColumnAfterCurrent( nullptr )
    , m_actionRemoveCurrentColumn( nullptr )
	, m_currentRow( 0 )
	, m_currentColumn( 0 )
{
	ui->setupUi( this );

	QAbstractItemDelegate* oldDelegate = ui->tableView->itemDelegate();
	ui->tableView->setItemDelegate( new MyDelegate( ui->tableView ) );
	if ( oldDelegate != nullptr )
	{
		delete oldDelegate;
		oldDelegate = nullptr;
	}

	// TODO: Qt does not provide any information about the future fate of the existing header
	// Should we delete it as the delegate or it will be destroyed the tableView?
	ui->tableView->setVerticalHeader( new EditableHeaderView( Qt::Vertical, ui->tableView ) );
	ui->tableView->setHorizontalHeader( new EditableHeaderView( Qt::Horizontal, ui->tableView ) );

	ui->tableView->verticalHeader()->setSectionResizeMode( QHeaderView::Fixed );
	ui->tableView->horizontalHeader()->setDefaultSectionSize( defaultSectionSize );
	ui->tableView->verticalHeader()->setEditTriggers( QAbstractItemView::DoubleClicked );

    m_actionInsertRowBeforeCurrent = m_menu->addAction( iconFromTheme( QStringLiteral( "list-add" ) ), tr( "Insert Row Before Current" ) ) ;
    m_actionInsertRowAfterCurrent = m_menu->addAction( iconFromTheme( QStringLiteral( "list-add" ) ), tr( "Insert Row After Current" ) ) ;
    m_actionRemoveCurrentRow = m_menu->addAction( iconFromTheme( QStringLiteral( "list-remove" ) ), tr( "Remove Current Row" ) ) ;
    m_actionInsertColumnBeforeCurrent = m_menu->addAction( iconFromTheme( QStringLiteral( "list-add" ) ), tr( "Insert Column Before Current" ) ) ;
    m_actionInsertColumnAfterCurrent = m_menu->addAction( iconFromTheme( QStringLiteral( "list-add" ) ), tr( "Insert Column After Current" ) ) ;
    m_actionRemoveCurrentColumn = m_menu->addAction( iconFromTheme( QStringLiteral( "list-remove" ) ), tr( "Remove Current Column" ) ) ;
	
	ui->tableView->viewport()->setContextMenuPolicy( Qt::CustomContextMenu );
	connect( ui->tableView, &QWidget::customContextMenuRequested
			, this, &TableEditor::onCustomContextMenuRequestedAtTableView );
	
	ui->tableView->horizontalHeader()->setContextMenuPolicy( Qt::CustomContextMenu );
	connect( ui->tableView->horizontalHeader(), &QWidget::customContextMenuRequested
			, this, &TableEditor::onCustomContextMenuRequestedAtHorizontalHeader );
	
	ui->tableView->verticalHeader()->setContextMenuPolicy( Qt::CustomContextMenu );
	connect( ui->tableView->verticalHeader(), &QWidget::customContextMenuRequested
			, this, &TableEditor::onCustomContextMenuRequestedAtVerticalHeader );

	connect( m_actionInsertRowBeforeCurrent, &QAction::triggered, this, &TableEditor::insertRowBeforeCurrent );
	connect( m_actionInsertRowAfterCurrent, &QAction::triggered, this, &TableEditor::insertRowAfterCurrent );
	connect( m_actionRemoveCurrentRow, &QAction::triggered, this, &TableEditor::removeCurrentRow );
	connect( m_actionInsertColumnBeforeCurrent, &QAction::triggered, this, &TableEditor::insertColumnBeforeCurrent );
	connect( m_actionInsertColumnAfterCurrent, &QAction::triggered, this, &TableEditor::insertColumnAfterCurrent );
	connect( m_actionRemoveCurrentColumn, &QAction::triggered, this, &TableEditor::removeCurrentColumn );
}

//--------------------------------------------------------------------------------

TableEditor::~TableEditor()
{
	delete ui;
	ui = nullptr;
}

//--------------------------------------------------------------------------------

void TableEditor::setModel( QAbstractItemModel* model )
{
	if ( m_model == model )
		return;

	m_model = model;

	if ( ui->tableView->selectionModel() != nullptr )
	{
		delete ui->tableView->selectionModel();
	}

	ui->tableView->setModel( m_model );
	//ui->tableView->resizeColumnsToContents();
}

//--------------------------------------------------------------------------------

void TableEditor::onCustomContextMenuRequestedAtVerticalHeader( const QPoint& pos )
{
	m_currentRow = ui->tableView->verticalHeader()->logicalIndexAt( pos );
	const bool moreRowsAreAllowed = m_model->rowCount() <= maxRowCount;

	if ( m_currentRow < 0 )
		return;

	m_actionInsertRowBeforeCurrent->setVisible( moreRowsAreAllowed );
	m_actionInsertRowAfterCurrent->setVisible( moreRowsAreAllowed );
	m_actionRemoveCurrentRow->setVisible( m_model->rowCount() > 1 );
	m_actionInsertColumnBeforeCurrent->setVisible( false );
	m_actionInsertColumnAfterCurrent->setVisible( false );
	m_actionRemoveCurrentColumn->setVisible( false );

	m_menu->exec( ui->tableView->verticalHeader()->mapToGlobal( pos ) );
}

//--------------------------------------------------------------------------------

void TableEditor::onCustomContextMenuRequestedAtHorizontalHeader( const QPoint& pos )
{
	m_currentColumn = ui->tableView->horizontalHeader()->logicalIndexAt( pos );

	if ( m_currentColumn < 0 )
		return;

	m_actionInsertRowBeforeCurrent->setVisible( false );
	m_actionInsertRowAfterCurrent->setVisible( false );
	m_actionRemoveCurrentRow->setVisible( false );
	m_actionInsertColumnBeforeCurrent->setVisible( true );
	m_actionInsertColumnAfterCurrent->setVisible( true );
	m_actionRemoveCurrentColumn->setVisible( m_model->columnCount() > 1 );

	m_menu->exec( ui->tableView->horizontalHeader()->mapToGlobal( pos ) );
}

//--------------------------------------------------------------------------------

void TableEditor::onCustomContextMenuRequestedAtTableView( const QPoint& pos )
{
	QModelIndex index = ui->tableView->indexAt( pos );
	m_currentRow = index.row();
	m_currentColumn = index.column();
	const bool moreRowsAreAllowed = m_model->rowCount() <= maxRowCount;

	if ( !index.isValid() )
		return;

	m_actionInsertRowBeforeCurrent->setVisible( moreRowsAreAllowed );
	m_actionInsertRowAfterCurrent->setVisible( moreRowsAreAllowed );
	m_actionRemoveCurrentRow->setVisible( m_model->rowCount() > 1 );
	m_actionInsertColumnBeforeCurrent->setVisible( true );
	m_actionInsertColumnAfterCurrent->setVisible( true );
	m_actionRemoveCurrentColumn->setVisible( m_model->columnCount() > 1 );

	m_menu->exec( ui->tableView->viewport()->mapToGlobal( pos ) );
}

//--------------------------------------------------------------------------------

void TableEditor::insertRowBeforeCurrent()
{
	m_model->insertRow( m_currentRow );
	setRowToValue( m_currentRow, 0 );
}

//--------------------------------------------------------------------------------

void TableEditor::insertRowAfterCurrent()
{
	m_model->insertRow( m_currentRow + 1 );
	setRowToValue( m_currentRow + 1, 0 );
}

//--------------------------------------------------------------------------------

void TableEditor::removeCurrentRow()
{
	m_model->removeRow( m_currentRow );
}

//--------------------------------------------------------------------------------

void TableEditor::insertColumnBeforeCurrent()
{
	m_model->insertColumn( m_currentColumn );
	setColumnToValue( m_currentColumn, 0 );
}

//--------------------------------------------------------------------------------

void TableEditor::insertColumnAfterCurrent()
{
	m_model->insertColumn( m_currentColumn + 1 );
	setColumnToValue( m_currentColumn + 1, 0 );
}

//--------------------------------------------------------------------------------

void TableEditor::removeCurrentColumn()
{
	m_model->removeColumn( m_currentColumn );
}

//--------------------------------------------------------------------------------

void TableEditor::setRowToValue( int row, int value )
{
	for ( int column = 0; column < m_model->columnCount(); ++column )
	{
		const QModelIndex index = m_model->index( row, column );
		m_model->setData( index, value, Qt::EditRole );
		m_model->setData( index, value, Qt::DisplayRole );
	}
}

//--------------------------------------------------------------------------------

void TableEditor::setColumnToValue( int column, int value )
{
	for ( int row = 0; row < m_model->rowCount(); ++row )
	{
		const QModelIndex index = m_model->index( row, column );
		m_model->setData( index, value, Qt::EditRole );
		m_model->setData( index, value, Qt::DisplayRole );
	}
}
