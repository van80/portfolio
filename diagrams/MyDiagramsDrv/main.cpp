//#include <iostream>
//#include <numeric>
//#include <vector>
//#include <iterator>
//#include <algorithm>

#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>

#include <MainWindow.hpp>


int main( int argc, char** argv )
{
	QApplication app(argc, argv);
	app.setOrganizationName( "nechaev_demo_application" );

	srand( time(NULL) );

	MainWindow mw;
	mw.show();

	int ret_val = app.exec();

	return ret_val;
}
