#pragma once

#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>

class EditableHeaderView : public QHeaderView
{
Q_OBJECT

public: // types

	typedef QHeaderView BaseClass;

public: // methods

	EditableHeaderView( Qt::Orientation orientation, QWidget* parent = nullptr );

protected:

	void mouseDoubleClickEvent( QMouseEvent* event ) override;

	bool eventFilter( QObject* object, QEvent* event ) override;

private Q_SLOTS:

	void commitAndCloseEditor();

private: // methods

	Q_DISABLE_COPY( EditableHeaderView );

	void commitData();

	void closeEditor();

private: // fields
	
	QLineEdit* m_textEdit;
	int m_sectionBeingEdited;

}; // class EditableHeaderView
