#pragma once

/*
 * In this file we gather all the constants used by BarDiagram
 * Some of them need to be reconsidered in the future
 */

namespace BarDiagramConstants {

	const qreal defaultDiagramAreaHeight = 300; // Diagram area exluding labels along the axises
	const qreal defaultDiagramAreaWidth = 400; // Diagram area exluding labels along the axises
	const qreal defaultMaxBarWidth = 30; // TODO: probably we should replace it by a function from font metrics
	const qreal defaultBarSpacing = 20; // inter-bar
	const qreal defaultHorPadding = 20; // before first, after last bar
	const qreal defaultBarAxisXDist = 10; // between axisX and bars
	const qreal defaultBarAxisYDist = 10; // between axisY and bars

	// requirements to the theme constants:
	// 1. themeColorCount * themeColorStep <= 256
	// 2. themeRedColorStart < themeColorStep
	// 3. themeGreenColorStart < themeColorStep
	// 4. themeBlueColorStart < themeColorStep
	const int themeColorCount = 64;
	const int themeColorStep = 4;
	const int themeRedColorStart = 1;
	const int themeGreenColorStart = 2;
	const int themeBlueColorStart = 3;

} // namespace constants
