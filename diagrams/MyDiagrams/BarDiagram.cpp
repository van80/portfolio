#include <BarDiagram.hpp>
#include <ui_BarDiagram.h>

#include <algorithm>

#include <QtSvg/QSvgGenerator>

#include <BarDiagramConstants.hpp>
#include <GraphicsBarItem.hpp>

typedef BarDiagram::ColorList ColorList;

namespace {

typedef BarDiagram::Bar Bar;
typedef BarDiagram::BarColumn BarColumn;
typedef BarDiagram::BarTable BarTable;

//--------------------------------------------------------------------------------

std::vector< int > random_int_array( int start, int count, int step )
{
	int idx = start;
	std::vector< int > v;
	v.resize( count );
	std::generate( v.begin(), v.end(), [step, &idx](){ int v = idx; idx += step; return v; } );

	std::random_shuffle( v.begin(), v.end() );

	return std::move( v );
}

//--------------------------------------------------------------------------------

// guarantees that all generated colors are different
ColorList generateRandomColorScheme( const QColor& mix = Qt::white  )
{
	const int colorCount = BarDiagramConstants::themeColorCount;
	const int colorStep = BarDiagramConstants::themeColorStep;

	std::vector< int > red_array = random_int_array( BarDiagramConstants::themeRedColorStart, colorCount, colorStep );
	std::vector< int > green_array = random_int_array( BarDiagramConstants::themeGreenColorStart, colorCount, colorStep);
	std::vector< int > blue_array = random_int_array( BarDiagramConstants::themeBlueColorStart, colorCount, colorStep );

	ColorList colors;
	colors.reserve( colorCount );
	for ( int k = 0; k < colorCount; ++k )
	{
		const int red = ( red_array.at( k ) + mix.red() ) / 2;
		const int green = ( green_array.at( k ) + mix.green() ) / 2;
		const int blue = ( blue_array.at( k ) + mix.blue() ) / 2;

		QColor color( red, green, blue );
		colors.push_back( color );
	}

	return colors;
}

//--------------------------------------------------------------------------------

qreal barHeightData( const QAbstractItemModel* model, int row, int column )
{
	return model->data( model->index( row, column ), Qt::EditRole ).toDouble(); 
}
	

} // private namespace





//--------------------------------------------------------------------------------

BarDiagram::BarDiagram( QWidget* parent )
	: BaseClass( parent )
	, ui( new Ui::BarDiagram() )
	, m_scene( new QGraphicsScene( this ) )
	, m_axisX( nullptr )
	, m_axisY( nullptr )
	, m_model( nullptr )
{
	ui->setupUi( this );

	setupScene();

	ui->graphicsView->setScene( m_scene );

	m_colorScheme = generateRandomColorScheme( palette().base().color() );
}

//--------------------------------------------------------------------------------

BarDiagram::~BarDiagram()
{
	// Bars have a slightly different ownership model than labels
	// We need to destroy them explicitly
	// The other scene objects will be eliminated by the scene itself
	destroyBars();

	delete ui;
	ui = nullptr;
}

//--------------------------------------------------------------------------------

void BarDiagram::setupScene()
{
	QPen pen( palette().text().color() );
	m_axisX = m_scene->addLine( 0, 0, 290, 0, pen );
	m_axisY = m_scene->addLine( 0, 0, 0, -205, pen );
	m_axisY->setVisible( false );
}

//--------------------------------------------------------------------------------

void BarDiagram::setModel( QAbstractItemModel* model )
{
	if ( m_model == model )
		return;

	if ( m_model != nullptr )
	{
		destroySceneObjects();

		disconnect( m_model, &QAbstractItemModel::dataChanged, this, &BarDiagram::onDataChanged );
		disconnect( m_model, &QAbstractItemModel::headerDataChanged, this, &BarDiagram::onHeaderDataChanged );
		disconnect( m_model, &QAbstractItemModel::rowsInserted, this, &BarDiagram::onRowsInserted );
		disconnect( m_model, &QAbstractItemModel::rowsRemoved, this, &BarDiagram::onRowsRemoved );
		disconnect( m_model, &QAbstractItemModel::columnsInserted, this, &BarDiagram::onColumnsInserted );
		disconnect( m_model, &QAbstractItemModel::columnsRemoved, this, &BarDiagram::onColumnsRemoved );
	}

	m_model = model;

	// New model means we need to create a completely new plot. 
	// We do it with default settings;
	m_settings = BarDiagramSettings(); 

	if ( m_model != nullptr )
	{
		buildSceneObjects();
		updateSceneObjectSizes();

		connect( m_model, &QAbstractItemModel::dataChanged, this, &BarDiagram::onDataChanged );
		connect( m_model, &QAbstractItemModel::headerDataChanged, this, &BarDiagram::onHeaderDataChanged );
		connect( m_model, &QAbstractItemModel::rowsInserted, this, &BarDiagram::onRowsInserted );
		connect( m_model, &QAbstractItemModel::rowsRemoved, this, &BarDiagram::onRowsRemoved );
		connect( m_model, &QAbstractItemModel::columnsInserted, this, &BarDiagram::onColumnsInserted );
		connect( m_model, &QAbstractItemModel::columnsRemoved, this, &BarDiagram::onColumnsRemoved );
	}
}

//--------------------------------------------------------------------------------

QColor BarDiagram::rowColor( int row ) const
{
	int idx = row % m_colorScheme.size();
	return m_colorScheme.at( idx );
}

//--------------------------------------------------------------------------------

void BarDiagram::destroyBars()
{
	std::for_each( m_barTable.begin(), m_barTable.end(), []( const BarColumn& column ){ qDeleteAll( column ); } );
	m_barTable.clear();
}	


//--------------------------------------------------------------------------------
void BarDiagram::destroySceneObjects() // leave only the axises
{
	destroyBars();

	std::for_each( m_verticalLabels.begin(), m_verticalLabels.end(), [this]( QGraphicsTextItem* item ){ m_scene->removeItem( item ); } );
	qDeleteAll( m_verticalLabels );
	m_verticalLabels.clear();

	std::for_each( m_verticalLabels.begin(), m_verticalLabels.end(), [this]( QGraphicsTextItem* item ){ m_scene->removeItem( item ); } );
	qDeleteAll( m_verticalLabels );
	m_verticalLabels.clear();

}
	
//--------------------------------------------------------------------------------

void BarDiagram::buildSceneObjects()
{
	m_barTable.reserve( m_model->columnCount() );

	for ( int column = 0; column < m_model->columnCount(); ++column )
	{
		BarColumn barColumn;
		barColumn.reserve( m_model->rowCount() );

		for ( int row = m_model->rowCount() - 1; row >= 0; --row )
		{
			Bar* bar = new Bar( m_scene );
			barColumn.push_back( bar );
		}

		m_barTable.push_back( barColumn );
	}

	for ( int column = 0; column < m_model->columnCount(); ++column )
	{
		QGraphicsTextItem* textItem = m_scene->addText( QString() );
		m_horizontalLabels.push_back( textItem );
	}

	for ( int row = m_model->rowCount() - 1; row >= 0; --row )
	{
		QGraphicsTextItem* textItem = m_scene->addText( QString() );
		m_verticalLabels.push_back( textItem );
	}

	std::reverse( m_verticalLabels.begin(), m_verticalLabels.end() );
}

//--------------------------------------------------------------------------------
	
void BarDiagram::rebuildSceneObjects()
{
	destroySceneObjects();
	buildSceneObjects();
}

//--------------------------------------------------------------------------------
	
void BarDiagram::updateSceneObjectSizes()
{
	const QPen pen( palette().text().color() );
	const qreal horPadding = m_settings.horizontalPadding(); // before first, after last
	const qreal barAxisXDist = m_settings.barAxisXDistance(); // between axisX and bars
	const qreal barAxisYDist = m_settings.barAxisYDistance(); // between axisY and bars
	const qreal maxBarWidth = m_settings.maxBarWidth();
	const qreal maxBarSpacing = m_settings.barSpacing();

	const qreal clientAreaHeight = m_settings.diagramAreaHeight() - 2 * barAxisXDist;
	const qreal clientAreaMaxWidth = m_settings.diagramAreaWidth() - 2 * horPadding;

	qreal columnMaxHeight = calcMaxColumnHeight();

	const qreal desiredClientWidth = m_model->columnCount() * maxBarWidth + (m_model->columnCount() - 1) * maxBarSpacing;
	const qreal clientAreaWidth = std::min( clientAreaMaxWidth, desiredClientWidth );

	const qreal scaleY = clientAreaHeight / columnMaxHeight;
	const qreal scaleX = clientAreaWidth / desiredClientWidth; // 0 < scaleX <= 1 because clientAreaWidth <= desiredClientwidth
	const qreal barWidth = maxBarWidth * scaleX;
	const qreal barSpacing = maxBarSpacing * scaleX;

	const int totalWidth = 2 * horPadding + clientAreaWidth;
	const int totalHeight = clientAreaHeight + 2 * barAxisXDist;

	const qreal maxY = 0;
	qreal x = horPadding;

	for ( int column = 0; column < m_model->columnCount(); ++column )
	{
		int y = maxY - barAxisXDist;
		const BarColumn& barColumn = m_barTable.at( column );

		for ( int row = m_model->rowCount() - 1; row >= 0; --row )
		{
			const qreal value = barHeightData( m_model, row, column );
			const qreal desiredBarHeight = value;
			const qreal barHeight = desiredBarHeight * scaleY;

			y -= barHeight;

			Bar* bar = barColumn.at( row );
			bar->setPen( pen );
			bar->setBarBrush( rowColor( row ) );
			bar->setValue( value );
			bar->setBarRect( QRectF( x, y, barWidth, barHeight ) );
		}

		x += barWidth + barSpacing;
	}

	// update axises

	m_axisX->setLine( 0, 0, totalWidth, 0 );
	m_axisY->setLine( 0, 0, 0, -totalHeight );

	// update labels

	updateHorizontalLabels( barWidth, barSpacing );

	updateVerticalLabels( scaleY );
}

//--------------------------------------------------------------------------------

qreal BarDiagram::calcMaxColumnHeight() const
{
	qreal columnMaxHeight = 0;

	// collect statistics
	for ( int column = 0; column < m_model->columnCount(); ++column )
	{
		qreal columnHeight = 0;

		for ( int row = 0; row < m_model->rowCount(); ++row )
		{
			columnHeight += barHeightData( m_model, row, column );
		}

		if ( columnHeight > columnMaxHeight )
		{
			columnMaxHeight = columnHeight;
		}
	}

	return columnMaxHeight;
}

//--------------------------------------------------------------------------------

void BarDiagram::updateHorizontalLabels( qreal barWidth, qreal barSpacing )
{
	const qreal maxY = 0;
	const qreal horPadding = m_settings.horizontalPadding(); // before first, after last
	const qreal barAxisXDist = m_settings.barAxisXDistance(); // between axisX and bars

	qreal x = horPadding;

	for ( int column = 0; column < m_model->columnCount(); ++column )
	{
		QString label = m_model->headerData( column, Qt::Horizontal ).toString();

		QGraphicsTextItem* textItem = m_horizontalLabels.at( column );
		textItem->setPlainText( label );
		const QSizeF textSize = textItem->boundingRect().size();
		const qreal labelX = x + (barWidth - textSize.width()) / 2;
		const qreal labelY = maxY + barAxisXDist;
		textItem->setPos( labelX, labelY );

		x += barWidth + barSpacing;
	}
}

//--------------------------------------------------------------------------------

void BarDiagram::updateVerticalLabels( qreal scaleY )
{
	const qreal maxY = 0;
	const qreal barAxisXDist = m_settings.barAxisXDistance(); // between axisX and bars
	const qreal barAxisYDist = m_settings.barAxisYDistance(); // between axisX and bars
	const qreal horPadding = m_settings.horizontalPadding(); // before first, after last

	int y = maxY - barAxisXDist;

	for ( int row = m_model->rowCount() - 1; row >= 0; --row )
	{
		QString label = m_model->headerData( row, Qt::Vertical ).toString();

		const qreal value = barHeightData( m_model, row, 0 );
		const qreal desiredBarHeight = value;
		const qreal barHeight = desiredBarHeight * scaleY;

		y -= barHeight;

		QGraphicsTextItem* textItem = m_verticalLabels.at( row );
		textItem->setPlainText( label );
		const QSizeF textSize = textItem->boundingRect().size();
		const qreal labelX = horPadding - barAxisYDist - textSize.width();
		const qreal labelY = y + (barHeight - textSize.height()) / 2;
		textItem->setPos( labelX, labelY );

	}
}

//--------------------------------------------------------------------------------

void BarDiagram::calculateRectangles( QRect& sourceRect, QRect& targetRect ) const
{
	QRectF boundingBox = m_scene->itemsBoundingRect();
	const int padding = 10;
	QPoint tl = ui->graphicsView->mapFromScene( boundingBox.topLeft() ) - QPoint( padding, padding );
	QPoint rb = ui->graphicsView->mapFromScene( boundingBox.bottomRight() ) + QPoint( padding, padding );
	sourceRect = QRect( tl, rb ) ;
	targetRect = QRect( 0, 0, sourceRect.width(), sourceRect.height() );
}

//--------------------------------------------------------------------------------

void BarDiagram::writeTo( QSvgGenerator* svgGenerator ) const
{
	QRect sourceRect, targetRect;
	calculateRectangles( sourceRect, targetRect );

	svgGenerator->setSize( sourceRect.size() );
	svgGenerator->setViewBox( QRect( targetRect ) );
	svgGenerator->setTitle( tr( "Histogram example" ) );
	svgGenerator->setDescription( tr( "SVG drawing created from the histogram provided by user." ) );

	QPainter painter( svgGenerator );
	ui->graphicsView->render( &painter, targetRect, sourceRect );
}

//--------------------------------------------------------------------------------

QPixmap BarDiagram::toPixmap() const
{
	QRect sourceRect, targetRect;
	calculateRectangles( sourceRect, targetRect );

	QPixmap pixmap( targetRect.size() );
	//pixmap.fill( Qt::transparent );
	pixmap.fill( palette().base().color() );

	QPainter painter( &pixmap );
	ui->graphicsView->render( &painter, targetRect, sourceRect );

	return pixmap;
}

//--------------------------------------------------------------------------------

void BarDiagram::gotoCenter()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::shiftLeft()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::shiftRight()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::shiftUp()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::shiftDown()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::zoomIn()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::zoomOut()
{
	// TODO: feature is not yet implemented!
}

//--------------------------------------------------------------------------------

void BarDiagram::onDataChanged( const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector< int >& roles )
{
	Q_UNUSED( topLeft );
	Q_UNUSED( bottomRight );
	Q_UNUSED( roles );
	updateSceneObjectSizes();
}

//--------------------------------------------------------------------------------

void BarDiagram::onHeaderDataChanged( Qt::Orientation orientation, int first, int last )
{
	Q_UNUSED( orientation );
	Q_UNUSED( first );
	Q_UNUSED( last );
	updateSceneObjectSizes();
}

//--------------------------------------------------------------------------------

void BarDiagram::onRowsInserted( const QModelIndex& parent, int first, int last )
{
	// we rotate the colors a little because we don't want the existing bars changing their colors
	auto begin = std::next( m_colorScheme.begin(), first );
	auto mid = std::next( m_colorScheme.begin(), m_barTable.size() );
	auto end = std::next( m_colorScheme.begin(), m_barTable.size() + last - first + 1 );
	std::rotate( begin, mid, end );

	rebuildSceneObjects();
	updateSceneObjectSizes();
}

//--------------------------------------------------------------------------------

void BarDiagram::onColumnsInserted( const QModelIndex& parent, int first, int last )
{
	rebuildSceneObjects();
	updateSceneObjectSizes();
}

//--------------------------------------------------------------------------------

void BarDiagram::onRowsRemoved( const QModelIndex& parent, int first, int last )
{
	// we rotate the colors a little because we don't want the existing bars changing their colors
	auto begin = std::next( m_colorScheme.begin(), first );
	auto mid = std::next( m_colorScheme.begin(), last + 1 );
	auto end = std::next( m_colorScheme.begin(), m_barTable.size() );
	std::rotate( begin, mid, end );

	rebuildSceneObjects();
	updateSceneObjectSizes();
}

//--------------------------------------------------------------------------------

void BarDiagram::onColumnsRemoved( const QModelIndex& parent, int first, int last )
{
	rebuildSceneObjects();
	updateSceneObjectSizes();
}
