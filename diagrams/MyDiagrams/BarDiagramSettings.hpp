#pragma once

#include <QtCore/QtCore>

class BarDiagramSettings
{

public: // methods

	BarDiagramSettings();

	~BarDiagramSettings();

	qreal diagramAreaWidth() const { return m_diagramAreaWidth; }

	void setDiagramAreaWidth( qreal value );

	qreal diagramAreaHeight() const { return m_diagramAreaHeight; }

	void setDiagramAreaHeight( qreal value );

	qreal maxBarWidth() const { return m_maxBarWidth; }

	void setMaxBarWidth( qreal value );

	qreal barSpacing() const { return m_barSpacing; }

	void setBarSpacing( qreal value );

	qreal horizontalPadding() const { return m_horPadding; }

	void setHorizontalPadding( qreal value );

	qreal barAxisXDistance() const { return m_barAxisXDist; }

	void setBarAxisXDistance( qreal value );

	qreal barAxisYDistance() const { return m_barAxisYDist; }

	void setBarAxisYDistance( qreal value );

private: // fields

	qreal m_diagramAreaWidth;
	qreal m_diagramAreaHeight;
	qreal m_maxBarWidth;
	qreal m_barSpacing; // inter-bar
	qreal m_horPadding; // before first, after last
	qreal m_barAxisXDist; // between axisX and bars
	qreal m_barAxisYDist; // between axisY and bars

}; // class BarDiagramSettings
