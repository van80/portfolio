#include <GraphicsBarItem.hpp>

#include <QGraphicsScene>

//--------------------------------------------------------------------------------

GraphicsBarItem::GraphicsBarItem( QGraphicsScene* scene )
	: m_scene( scene )
	, m_value( 0 )
	, m_barItem( nullptr )
	, m_barFrameItem( nullptr )
	, m_textItem( nullptr )
	, m_textFrameItem( nullptr )
	, m_textMoveHandleItem( nullptr )
	, m_textBarConnectorItem( nullptr )
{
	init( scene );
}

//--------------------------------------------------------------------------------

GraphicsBarItem::GraphicsBarItem( QGraphicsScene* scene, qreal value, const QRectF& barRect, const QPen& pen, const QBrush& brush )
	: m_scene( scene )
	, m_value( value )
	, m_barRect( barRect )
	, m_pen( pen )
	, m_barBrush( brush )
	, m_barItem( nullptr )
	, m_barFrameItem( nullptr )
	, m_textItem( nullptr )
	, m_textFrameItem( nullptr )
	, m_textMoveHandleItem( nullptr )
	, m_textBarConnectorItem( nullptr )
{
	init( scene );
}

//--------------------------------------------------------------------------------

GraphicsBarItem::~GraphicsBarItem()
{
	m_scene->removeItem( m_barItem );
	m_scene->removeItem( m_textItem );

	delete m_barItem;
	m_barItem = nullptr;

	delete m_textItem;
	m_textItem = nullptr;
}

//--------------------------------------------------------------------------------

void GraphicsBarItem::init( QGraphicsScene* scene )
{
	m_barItem = scene->addRect( m_barRect, m_pen, m_barBrush );

	m_textItem = scene->addText( QStringLiteral( "" ) );

	updateLabel();
}

//--------------------------------------------------------------------------------

void GraphicsBarItem::setValue( qreal value )
{
	if ( m_value == value )
		return;

	m_value = value;

	updateLabel();
}

//--------------------------------------------------------------------------------

void GraphicsBarItem::setBarRect( const QRectF& value )
{
	if ( m_barRect == value )
		return;

	m_barRect = value;

	m_barItem->setRect( m_barRect );

	updateLabel();
}

//--------------------------------------------------------------------------------

void GraphicsBarItem::setPen( const QPen& value )
{
	if ( m_pen == value )
		return;

	m_pen = value;

	m_barItem->setPen( m_pen );
}

//--------------------------------------------------------------------------------

void GraphicsBarItem::setBarBrush( const QBrush& value )
{
	if ( m_barBrush == value )
		return;

	m_barBrush = value;

	m_barItem->setBrush( m_barBrush );
}

//--------------------------------------------------------------------------------

void GraphicsBarItem::updateLabel()
{
	m_textItem->setPlainText( QString( "%1" ).arg( static_cast< int >( std::round( m_value ) ) ) );

	const QSizeF textSize = m_textItem->boundingRect().size();

	m_textItem->setVisible( (textSize.height() / 2) < m_barRect.height() );

	m_textItem->setPos( m_barRect.center().x() - (textSize.width() / 2.0), m_barRect.center().y() - (textSize.height() / 2.0) );
}
