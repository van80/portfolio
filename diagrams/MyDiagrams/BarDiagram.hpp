#pragma once

#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>

#include <BarDiagramSettings.hpp>


class QSvgGenerator;
class GraphicsBarItem;

namespace Ui {
	class BarDiagram;
}

//================================================================================

class BarDiagram : public QWidget
{
Q_OBJECT 

public: // types

	typedef QWidget BaseClass;
	typedef GraphicsBarItem Bar;
	typedef QList< Bar* > BarColumn;
	typedef QList< BarColumn > BarTable;
	typedef QList< QGraphicsTextItem* > LabelList;
	typedef QList< QColor > ColorList;

public: // methods

	explicit BarDiagram( QWidget* parent = nullptr );

	~BarDiagram();

	//! Save the contents in SVG format
	void writeTo( QSvgGenerator* svgGenerator ) const;

	//! Save the contents in raster format
	QPixmap toPixmap() const;

	QAbstractItemModel* model() const { return m_model; }

public Q_SLOTS:

	void setModel( QAbstractItemModel* model );

	void gotoCenter();

	void shiftLeft();

	void shiftRight();

	void shiftUp();

	void shiftDown();

	void zoomIn();

	void zoomOut();

protected: // methods

private Q_SLOTS:

	void onDataChanged( const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector< int >& roles );

	void onHeaderDataChanged( Qt::Orientation orientation, int first, int last );

	void onRowsInserted( const QModelIndex& parent, int first, int last );

	void onColumnsInserted( const QModelIndex& parent, int first, int last );

	void onRowsRemoved( const QModelIndex& parent, int first, int last );

	void onColumnsRemoved( const QModelIndex& parent, int first, int last );

private: // methods
	
	Q_DISABLE_COPY( BarDiagram )

	void setupScene();

	void destroyBars();
	
	void destroySceneObjects(); // leave only the axises

	void calculateRectangles( QRect& sourceRect, QRect& targetRect ) const;
	
	void updateSceneObjectSizes();

	void updateHorizontalLabels( qreal barWidth, qreal barSpacing );

	void updateVerticalLabels( qreal scaleY );
	
	void rebuildSceneObjects();
	
	void buildSceneObjects();

	QColor rowColor( int row ) const;

	qreal calcMaxColumnHeight() const;

private:

	Ui::BarDiagram* ui;

	QTransform m_transform;
	QGraphicsScene* m_scene;
	QGraphicsLineItem* m_axisX;
	QGraphicsLineItem* m_axisY;

	QAbstractItemModel* m_model;

	BarTable m_barTable;
	LabelList m_horizontalLabels;
	LabelList m_verticalLabels;
	
	ColorList m_colorScheme;

	BarDiagramSettings m_settings;

};
