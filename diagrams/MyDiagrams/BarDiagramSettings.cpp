#include <BarDiagramSettings.hpp>

#include <BarDiagramConstants.hpp>

//--------------------------------------------------------------------------------

BarDiagramSettings::BarDiagramSettings()
	: m_diagramAreaWidth( BarDiagramConstants::defaultDiagramAreaWidth )
	, m_diagramAreaHeight( BarDiagramConstants::defaultDiagramAreaHeight )
	, m_maxBarWidth( BarDiagramConstants::defaultMaxBarWidth )
	, m_barSpacing( BarDiagramConstants::defaultBarSpacing )
	, m_horPadding( BarDiagramConstants::defaultHorPadding )
	, m_barAxisXDist( BarDiagramConstants::defaultBarAxisXDist )
	, m_barAxisYDist( BarDiagramConstants::defaultBarAxisYDist )
{
}

//--------------------------------------------------------------------------------

BarDiagramSettings::~BarDiagramSettings()
{
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setDiagramAreaWidth( qreal value )
{
	m_diagramAreaWidth = value;
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setDiagramAreaHeight( qreal value )
{
	m_diagramAreaHeight = value;
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setMaxBarWidth( qreal value )
{
	m_maxBarWidth = value;
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setBarSpacing( qreal value )
{
	m_barSpacing = value;
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setHorizontalPadding( qreal value )
{
	m_horPadding = value;
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setBarAxisXDistance( qreal value )
{
	m_barAxisXDist = value;
}

//--------------------------------------------------------------------------------

void BarDiagramSettings::setBarAxisYDistance( qreal value )
{
	m_barAxisYDist = value;
}
