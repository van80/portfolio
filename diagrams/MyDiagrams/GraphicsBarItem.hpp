#pragma once

#include <QRectF>
//#include <QWidgets/QGraphicsItemGroup>
#include <QPen>
#include <QBrush>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QGraphicsPathItem>

class GraphicsBarItem //: public QGraphicsItemGroup
{
public: // types

	//QGraphicsItemGroup BaseClass;

public: // methods

	GraphicsBarItem( QGraphicsScene* scene );

	GraphicsBarItem( QGraphicsScene* scene, qreal value, const QRectF& barRect, const QPen& pen, const QBrush& brush );

	~GraphicsBarItem();

	QPen pen() const { return m_pen; }

	void setPen( const QPen& value );

	QBrush barBrush() const { return  m_barBrush; }

	void setBarBrush( const QBrush& value );

	qreal value() const { return m_value; }

	void setValue( qreal value );

	QRectF barRect() const { return m_barRect; }

	void setBarRect( const QRectF& value );

private: // methods
	
	Q_DISABLE_COPY( GraphicsBarItem )

	void init( QGraphicsScene* scene );

	void updateLabel();

private: // fields

	QGraphicsScene* m_scene;

	qreal m_value;
	QRectF m_barRect;
	QPen m_pen;
	QBrush m_barBrush;

	QGraphicsRectItem* m_barItem;
	QGraphicsRectItem* m_barFrameItem;
	QGraphicsTextItem* m_textItem;
	QGraphicsRectItem* m_textFrameItem;
	QGraphicsPathItem* m_textMoveHandleItem;
	QGraphicsLineItem* m_textBarConnectorItem;

}; // class GraphicsBarItem
